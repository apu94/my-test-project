pipeline {
    agent any
        parameters {
        choice(choices: ['','gas-uat','gas-prod'], description: 'Select environment for deploy API ?', name: 'DEPLOYMENT_ENV')
        choice(choices: ['','gmonkey-prod','gmonkey-uat'], description: 'Select Deploy NameSpace?', name: 'NAMESPACE')
        }

    environment {
        REGISTRY="docker-reg.oss.net.bd"
        DEPLOYMENT_ENV="$DEPLOYMENT_ENV"
        IMAGE_CREATED_BY="jenkins"
        PROJECT_NAME="gas-monkey-api"
        K8S_NAMESPACE="$NAMESPACE"

        //Port configuration
        K8S_CONTAINER_AND_TARGET_PORT="8801"
        K8S_SERVICE_PORT="8801"
        
        CONTAINER_PORT_NAME="gasm-port"
        SERVICE_PORT_NAME="http"
        
        //Memory allocation       
        K8S_MAX_RAM_POD="525Mi"

        //Application configurations and credentials
        K8S_CONFIGMAP_NAME="gas-monkey-cmp"
        K8S_SECRET_NAME="gas-monkey-sec"
        
     
        //Host specific configuration
        HOST_VOLUME_LOCATION="$JENKINS_DATA_LOCATION"
        M2_REPO="$JENKINS_DATA_LOCATION/.m2"

        PROJECT_LOCATION="$HOST_VOLUME_LOCATION/workspace/$JOB_NAME"
        IMAGE_VERSION="$BUILD_NUMBER-$IMAGE_CREATED_BY-$GIT_TAG-$DEPLOYMENT_ENV"
        IMAGE_REPOSITORY_NAME="$REGISTRY/$PROJECT_NAME"
        DOCKER_TAG="$IMAGE_REPOSITORY_NAME:$IMAGE_VERSION"
        DEPLOYMENT_DIRECTORY="./deploy/k8s/gas-monkey"
        
        //k8s cluster specific configuration
        K8S_SERVICE_NAME="$PROJECT_NAME"
        K8S_CHANGE_CAUSE="$IMAGE_VERSION"

        DEPLOYMENT_TIMEOUT="180s"

        dockerImage = ''
        dockerRegistryCredential='uat-docker-reg.oss.net.bd'
        DOCKER_REGISTRY_URL="https://$REGISTRY"

        NODE_PORT_UAT="31301"
        NODE_PORT_PROD="31401"
    }

    stages {
        stage('pre flight check') {
            steps {
                sh '''       
                if [ -z "$DEPLOYMENT_ENV" ] #empty check
                 then
                    echo ERROR: Please select aquirate Environment
                    exit 1 # terminate and indicate error                 
                fi
                echo "Applying API for $DEPLOYMENT_ENV"
                '''    
            }
        }
               

        stage('Test') {
            steps {
                sh '''
                echo "Node port value -------->$K8S_NODE_PORT jenkins location ==> $JENKINS_DATA_LOCATION job name $JOB_NAME tag name $GIT_TAG"
                '''
            }
        }

        stage('Maven Build') {
            steps {
                    sh '''
                    echo "release.version=Release Version is $IMAGE_VERSION" > src/main/resources/release-note.properties
                    echo "release.lastcommitid=Release Version is $(git log -1|head -1|awk -F ' ' '{print $NF}')" >> src/main/resources/release-note.properties
                    echo "Maven build for Tag.....$GIT_TAG"
                    docker run --rm \
                    -v $PROJECT_LOCATION:/app \
                    -v $M2_REPO:/root/.m2/ -w /app \
                    maven:3.8.4-eclipse-temurin-11-alpine \
                    mvn clean package -B \
                    -Dmaven.test.skip=true \
                    -Dactive.profile=$DEPLOYMENT_ENV
                    '''
                }      
    }

        stage('Building Docker image') { 
            steps { 
                script { 
                dockerImage = docker.build("$DOCKER_TAG", "-f $DEPLOYMENT_DIRECTORY/k8s.Dockerfile .")
                }
                sh '''
                docker images|grep $PROJECT_NAME
                '''
        } 
    }
    

        stage('Push docker image') {
            steps{
                script {
                    docker.withRegistry( "$DOCKER_REGISTRY_URL", dockerRegistryCredential ) {
                    dockerImage.push()
                    sh "docker images | grep $IMAGE_REPOSITORY_NAME"
                }
                
            }
        }
    }
        stage('Clear image from local after push to Registry') {
            steps {
                echo "Cleaning local docker registry $IMAGE_REPOSITORY_NAME"
                sh '''
                docker rmi $(docker images | grep $IMAGE_REPOSITORY_NAME | awk -F' ' '{print $3}')
                '''
        }
    }
        stage('Deploy Pod') {
            steps {
                sh '''
                envsubst < $DEPLOYMENT_DIRECTORY/$DEPLOYMENT_ENV/k8s-deploy.yaml | xargs -I{} echo {}
                envsubst < $DEPLOYMENT_DIRECTORY/$DEPLOYMENT_ENV/k8s-deploy.yaml | kubectl apply -f -
                kubectl rollout status -n $K8S_NAMESPACE deployment $PROJECT_NAME --timeout $DEPLOYMENT_TIMEOUT
                '''
        }
    }
        stage('Deploy Service') {
            steps {
                sh '''
                envsubst < $DEPLOYMENT_DIRECTORY/$DEPLOYMENT_ENV/k8s-service.yaml | xargs -I{} echo {}
                envsubst < $DEPLOYMENT_DIRECTORY/$DEPLOYMENT_ENV/k8s-service.yaml | kubectl apply -f -
                '''
               
                }
            } 
        }                    
    }
