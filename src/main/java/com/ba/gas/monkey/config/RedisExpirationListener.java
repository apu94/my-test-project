package com.ba.gas.monkey.config;

import com.ba.gas.monkey.dtos.partner.PartnerDetailsBean;
import com.ba.gas.monkey.models.OrderHistory;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.repositories.OrderRepository;
import com.ba.gas.monkey.services.OrderHistoryService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.notification.AppNotificationService;
import com.ba.gas.monkey.services.order.OrderService;
import com.ba.gas.monkey.utility.Constant;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@AllArgsConstructor
public class RedisExpirationListener implements MessageListener {
    private RedisTemplate<String, Object> template;
    //    private PartnerDetailsService partnerDetailsService;
    private OrderService orderService;
    //    private ModelMapper modelMapper;
    private OrderRepository orderRepository;
    private AppNotificationService appNotificationService;
    private CustomerService customerService;
    private OrderHistoryService historyService;


    @Override
    public void onMessage(Message message, byte[] bytes) {
        String body = new String(message.getBody());
        System.out.println("body:" + body);
        String lastPart = body.substring(body.lastIndexOf('&') + 1);
        System.out.println("lastPart:" + lastPart);
        String key = lastPart.substring(0, lastPart.length() - 2);
        System.out.println("key:" + key);
        int count = Integer.parseInt(lastPart.substring(lastPart.lastIndexOf('-') + 1));
        System.out.println("count:" + count);
        Optional<OrderInfo> optional = orderRepository.findById(key);
        if (optional.isEmpty()) {
            return;
        }
        OrderInfo orderInfo = optional.get();
        sendAdminNotification(orderInfo);
        if (count > 2) {
            if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.PENDING)) {
                orderInfo.setOrderStatus(Constant.NO_ACTION);
                orderInfo.setPartner(null);
            }
            orderRepository.save(orderInfo);
            System.out.println("change order status");
        } else {
            OrderInfo updatedOrderInfo = null;
            if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.PENDING)) {
                List<PartnerDetailsBean> nearByPartnerList = orderService.getNearByPartner(orderInfo, count);
                log.info("radis-Partner-found=============>>>{}", nearByPartnerList.size());
                if (nearByPartnerList.size() > 0) {
                    log.info("radis-Partner-found=============>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getId());
                    log.info("radis-Partner-found=============>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getCustomerName());
                    orderInfo.setPartner(customerService.getRepository().getById(nearByPartnerList.get(0).getCustomerInfoBean().getId()));
                    orderInfo.setOrderStatus(Constant.PENDING);
                    orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
                    updatedOrderInfo = orderRepository.save(orderInfo);
                    appNotificationService.sendOrderActionNotification(orderInfo, nearByPartnerList.get(0));
                    historyService.saveHistory(getOrderHistoryBean(updatedOrderInfo));
                } else {
                    if(count == 2){
                        sendAdminNotification(orderInfo);
                    }
                    orderInfo.setOrderStatus(Constant.NO_USER_FOUND);
                    orderInfo.setPartner(null);
                    orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
                    updatedOrderInfo = orderRepository.save(orderInfo);
                    historyService.saveHistory(getOrderHistoryBean(updatedOrderInfo));
                }
                count++;
                String newKey = key + "-" + count;
                template.opsForValue().set(newKey, key);
                template.expire(newKey, Constant.TTL, TimeUnit.MINUTES);
            } else if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.NO_USER_FOUND)) {
                List<PartnerDetailsBean> nearByPartnerList = orderService.getNearByPartner(orderInfo, 0);
                log.info("radis-Partner-found====1st=========>>>{}", nearByPartnerList.size());
                if (nearByPartnerList.size() > 0) {
                    log.info("radis-Partner-found====1st=========>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getId());
                    log.info("radis-Partner-found====1st=========>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getCustomerName());
                    orderInfo.setPartner(customerService.getRepository().getById(nearByPartnerList.get(0).getCustomerInfoBean().getId()));
                    orderInfo.setOrderStatus(Constant.PENDING);
                    orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
                    updatedOrderInfo = orderRepository.save(orderInfo);
                    historyService.saveHistory(getOrderHistoryBean(updatedOrderInfo));
                    appNotificationService.sendOrderActionNotification(orderInfo, nearByPartnerList.get(0));
                } else {
                    if(count == 2){
                        sendAdminNotification(orderInfo);
                    }
                    orderInfo.setOrderStatus(Constant.NO_USER_FOUND);
                    orderInfo.setPartner(null);
                    orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
                    updatedOrderInfo = orderRepository.save(orderInfo);
                    historyService.saveHistory(getOrderHistoryBean(updatedOrderInfo));
                }
                count++;
                String newKey = key + "-" + count;
                template.opsForValue().set(newKey, key);
                template.expire(newKey, Constant.TTL, TimeUnit.MINUTES);
            } else if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.CANCELED)) {
                appNotificationService.customerOrderCanceled(orderInfo);
                if (orderInfo.getPartner() != null) {
                    appNotificationService.partnerOrderCanceled(orderInfo);
                }
            }

        }
    }

    public void sendAdminNotification(OrderInfo orderInfo){
        appNotificationService.sendPartnerNotFoundNotification(orderInfo);
    }

    private OrderHistory getOrderHistoryBean(OrderInfo orderInfo) {
        OrderHistory historyBean = new OrderHistory();
        historyBean.setOrderStatus(orderInfo.getOrderStatus());
        historyBean.setPartner(orderInfo.getPartner());
        historyBean.setOrderInfo(orderInfo);
        historyBean.setStatus(Boolean.TRUE);
        historyBean.setDateCreated(Instant.now());
        historyBean.setDateModified(Instant.now());
        historyBean.setUpdtId(orderInfo.getUpdtId());
        return historyBean;
    }
}