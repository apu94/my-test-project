package com.ba.gas.monkey.security.customer;

import com.ba.gas.monkey.filter.AppLogTraceFilter;
import com.ba.gas.monkey.filter.AuthoritiesLoggingAfterFilter;
import com.ba.gas.monkey.filter.JWTTokenValidationFilter;
import com.ba.gas.monkey.security.CustomAuthenticationProvider;
import com.ba.gas.monkey.security.admin.AuthEntryPointJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Order(2)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CustomerSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String API_PATH = "/v*";

    @Autowired
    private AuthEntryPointJwt unauthorizedHandler;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JWTTokenValidationFilter authenticationJwtTokenFilter() {
        return new JWTTokenValidationFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher(API_PATH + "/customer/**")
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .addFilterAfter(new AppLogTraceFilter(), BasicAuthenticationFilter.class)
                .addFilterAt(new AuthoritiesLoggingAfterFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(API_PATH + "/customer/**").permitAll();
        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
