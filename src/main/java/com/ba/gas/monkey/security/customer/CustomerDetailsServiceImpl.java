package com.ba.gas.monkey.security.customer;

import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.services.customer.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@AllArgsConstructor
@Service("customerDetailsService")
public class CustomerDetailsServiceImpl implements UserDetailsService {

    private final CustomerService service;

    @Override
    public UserDetails loadUserByUsername(String mobileNo) throws UsernameNotFoundException {
        CustomerBean customer = service.getByPhoneNo(mobileNo)
                .orElseThrow(() -> new UsernameNotFoundException("No customer found by mobile no : " + mobileNo));
        return new CustomerDetailsImpl(customer);
    }

}
