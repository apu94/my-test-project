package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "dealer_details")
@EqualsAndHashCode(callSuper = true)
public class DealerDetails extends BaseEntity {
    @Column(name = "CUSTOMER_ID")
    private String customerId;
    @Column(name = "SHOP_NAME")
    private String shopName;
    @Column(name = "KEY_CONTACT_NAME")
    private String keyContactName;
    @Column(name = "KEY_CONTACT_PHONE")
    private String keyContactPhone;
}
