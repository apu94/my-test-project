package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "customer_attachment")
@EqualsAndHashCode(callSuper = true)
public class CustomerAttachment extends BaseEntity {
    @Column(name = "ATTACHMENT_ID")
    private String attachmentId;
    @Column(name = "CUSTOMER_ID")
    private String customerId;
    @Column(name = "ATTACHMENT_LINK")
    private String attachmentLink;
}
