package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "dealer_brands")
@EqualsAndHashCode(callSuper = true)
public class DealerBrand extends BaseEntity {
    @Column(name = "BRAND_ID")
    private String brandId;
    @Column(name = "DEALER_ID")
    private String dealerId;
}
