package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "push_notification")
@EqualsAndHashCode(callSuper = true)
public class PushNotification extends BaseEntity {
    @Column(name = "SUBJECT")
    private String subject;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "TEMPLATE_ID", referencedColumnName = "ID")
    private Template template;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "CUSTOMER_TYPE", referencedColumnName = "ID")
    private CustomerType customerType;
}
