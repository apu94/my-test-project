package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "notification")
@EqualsAndHashCode(callSuper = true)
public class Notification extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "TEMPLATE_ID", referencedColumnName = "ID")
    private Template template;
    @Column(name = "RECEIVER_ID")
    private String receiverId;
    @Column(name = "MODULE", columnDefinition = "ENUM")
    private String module;
    @Column(name = "DEVICE_TOKEN")
    private String deviceToken;
    @Column(name = "DESTINATION")
    private String destination;
    @Column(name = "NOTIFICATION_STATUS")
    private Integer notificationStatus;
    @Column(name = "MEDIA_TYPE", columnDefinition = "ENUM")
    private String mediaType;
    @Column(name = "NOTIFICATION_TYPE")
    private String notificationType;
    @Column(name = "SEEN")
    private Boolean seen;
    @Column(name = "NO_OF_TRY")
    private Integer noOfTry;
    @Column(name = "DEVICE_TYPE", columnDefinition = "ENUM")
    private String deviceType;
    @Column(name = "ORDER_ID")
    private String orderId;
    @JsonManagedReference
    @OneToOne(mappedBy = "notification", cascade = CascadeType.ALL)
    private NotificationDetails notificationDetails;
}
