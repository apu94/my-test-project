package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "partner_details")
@EqualsAndHashCode(callSuper = true)
public class PartnerDetails extends BaseEntity {
    @Column(name = "CUSTOMER_ID")
    private String customerId;
    @Column(name = "WORKING_ZONE")
    private String workingZone;
    @OneToOne
    @JoinColumn(name = "MOBILE_BANK_ID", referencedColumnName = "ID")
    private MobileBankingInfo mobileBankingInfo;
    @Column(name = "MOBILE_WALLET_NO")
    private String mobileWalletNo;
    @Column(name = "NOMINEE_NAME")
    private String nomineeName;
    @Column(name = "NOMINEE_PHONE")
    private String nomineePhone;
    @Column(name = "NOMINEE_RELATIONSHIP")
    private String nomineeRelationShip;
    @Column(name = "TOTAL_EARNINGS")
    private Double totalEarnings;
    @Column(name = "TOTAL_PAYABLE")
    private Double totalPayable;
}
