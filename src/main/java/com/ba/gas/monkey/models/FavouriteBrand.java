package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "favourite_brand")
@EqualsAndHashCode(callSuper = true)
public class FavouriteBrand extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "BRAND_ID", referencedColumnName = "ID")
    private Brand brand;
    @OneToOne
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID")
    private Customer customer;
}
