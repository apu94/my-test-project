package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "delivery_schedule")
@EqualsAndHashCode(callSuper = true)
public class DeliverySchedule extends BaseEntity {

    @Column(name = "START_TIME")
    private LocalTime startTime;
    @Column(name = "END_TIME")
    private LocalTime endTime;
    @Column(name = "SLOT_DURATION_IN_MIN")
    private Integer slotDurationInMin;
    @Column(name = "ENABLED_TIME_SLOT", columnDefinition = "BIT default 0", length = 1)
    private Boolean enabledTimeSlot;

}
