package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "device_token")
@EqualsAndHashCode(callSuper = true)
public class DeviceToken extends BaseEntity {
    @Column(name = "USER_ID")
    private String userId;
    @Column(name = "DEVICE_TYPE", columnDefinition = "ENUM")
    private String deviceType;
    @Column(name = "MODULE", columnDefinition = "ENUM")
    private String module;
    @Column(name = "TOKEN")
    private String token;
}
