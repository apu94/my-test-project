package com.ba.gas.monkey.dtos.cluster;

import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class ClusterListBean extends RowInfoBean {
    private String name;
    private String status;
}
