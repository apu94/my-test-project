package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.*;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderBean implements IRequestBodyDTO {
    private String id;
    private Customer customer;
    private Customer partner;
    private Customer dealer;
    private Boolean status;
    private String orderStatus;
    private Long orderNumber;
    private Double subTotal;
    private Double vat;
    private Double serviceCharge;
    private Double discountAmount;
    private Double total;
    private String couponCode;
    private Double couponValue;
    private String note;
    private Boolean regularDelivery;
    private LocalDate deliveryDate;
    private String deliverySlot;
    private String deliveryPaymentType;
    private String deliveryMapAddress;
    private Double deliveryLat;
    private Double deliveryLong;
    private District district;
    private Thana thana;
    private Cluster cluster;
    private String deliveryArea;
    private Boolean lift;
    private String floor;
    private List<OrderProduct> orderProductList;
    private Double vatPercent = 0.0;
    private Double exchange = 0.0;
    private Instant dateCreated;
    private Boolean isHome;
    private LocalDateTime acceptedAt;
    private LocalDateTime pickedAt;
    private LocalDateTime receivedAt;
    private LocalDateTime deliveredAt;
    private LocalDateTime cylinderReturnedAt;
    private Double partnerCharge;
    private Double ownerCharge;
    private Boolean paymentStatus;
    private Double totalExchange;
    private Double totalConvenienceFee;
}
