package com.ba.gas.monkey.dtos.customer;


import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class CustomerListBean {
    private String id;
    private Integer slNo;
    private String customerName;
    private String emailAddress;
    private String phoneNo;
    private String companyName;
    private String status;
    private String userStatus;
    private String area;
    private String address;
    private String lastOrder;
    private Integer totalOrderNo;
    private Double purchaseHistory;
    private Double rewardPoint;
    private String customerType;
    private String district;
    private String thana;
    private LocalDateTime lastAccess;
    private Long lastAccessInMillisecond;
    private Long pendingOrders;

}
