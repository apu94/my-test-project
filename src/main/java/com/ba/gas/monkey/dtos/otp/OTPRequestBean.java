package com.ba.gas.monkey.dtos.otp;

import com.ba.gas.monkey.validators.PhoneNumberValidation;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OTPRequestBean {
    @PhoneNumberValidation
    private String phoneNo;
    @NotBlank
    private String sessionId;
}
