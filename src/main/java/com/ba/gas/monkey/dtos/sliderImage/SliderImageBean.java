package com.ba.gas.monkey.dtos.sliderImage;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SliderImageBean implements IRequestBodyDTO {
    @NotBlank
    private String imageLink;
    @NotNull
    private Boolean status;
    @NotBlank
    private String title;
    private String imageLinkId;
}
