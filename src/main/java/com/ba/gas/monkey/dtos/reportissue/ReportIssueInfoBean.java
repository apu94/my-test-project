package com.ba.gas.monkey.dtos.reportissue;

import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.issuetype.IssueTypeBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.IssueType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class ReportIssueInfoBean {
    private String id;
    private Integer issueStatus;
    private String details;
    private String note;
    private IssueTypeBean issueType;
    private CustomerBean customerId;
}
