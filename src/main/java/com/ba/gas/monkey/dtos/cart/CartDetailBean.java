package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.product.ProductPriceBean;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CartDetailBean implements IRequestBodyDTO {
    private String id;
    private Cart cart;
    private Product productBuy;
    private Product productReturn;
    private Boolean status;
    private Integer quantity;
}
