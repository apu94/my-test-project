package com.ba.gas.monkey.dtos.product;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.ProductImage;
import com.ba.gas.monkey.models.ProductSize;
import com.ba.gas.monkey.models.ProductValveSize;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProductEditBean {
    private String id;
    @ValidEntityId(Brand.class)
    private String brandId;
    @ValidEntityId(ProductSize.class)
    private String productSizeId;
    @ValidEntityId(ProductValveSize.class)
    private String productValveSizeId;
    @NotNull
    private Boolean status;
    @NotBlank
    private String code;
    private Boolean featureProduct;
    private Boolean offerProduct;
    private ProductPriceBean productPriceBean;
    @NotNull
    private Double convenienceFee;
}
