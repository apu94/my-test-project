package com.ba.gas.monkey.dtos.thana;

import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class ThanaListBean extends RowInfoBean {
    private String name;
    private String status;
}
