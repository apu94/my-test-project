package com.ba.gas.monkey.dtos.dealer;

import lombok.Data;

@Data
public class DealerBrandListBean {

    private String brandId;
    private String brandName;

}
