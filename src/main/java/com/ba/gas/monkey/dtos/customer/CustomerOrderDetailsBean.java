package com.ba.gas.monkey.dtos.customer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerOrderDetailsBean {
    private Integer slNo;
    private String orderNumber;
    private String orderType;
    private String productName;
    private String emptyCylinderBrandName;
    private String deliveryStatus;
    private Double totalAmount;
}
