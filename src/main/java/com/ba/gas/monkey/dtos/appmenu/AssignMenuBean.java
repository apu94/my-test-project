package com.ba.gas.monkey.dtos.appmenu;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AssignMenuBean {
    private String menuId;
    private String tagName;
    private Boolean status;
}
