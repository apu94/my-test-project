package com.ba.gas.monkey.dtos.timeslot;

import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
public class TimeSlotBean {
    private String startTime;
    private String endTime;
}
