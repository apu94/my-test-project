package com.ba.gas.monkey.dtos.dealer;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.RowInfoBean;
import com.ba.gas.monkey.dtos.brand.BrandBean;
import com.ba.gas.monkey.dtos.customer.CustomerAttachmentBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class DealerDetailsBean extends RowInfoBean implements IRequestBodyDTO {
    private CustomerInfoBean customerInfoBean;
    @JsonIgnore
    private String customerId;
    private String shopName;
    private Boolean status;
    private String keyContactName;
    private String keyContactPhone;
    private List<CustomerAttachmentBean> attachmentBeans;
    private List<BrandBean> brands;
}
