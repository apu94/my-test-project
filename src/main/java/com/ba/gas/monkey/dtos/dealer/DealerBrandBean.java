package com.ba.gas.monkey.dtos.dealer;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class DealerBrandBean implements IRequestBodyDTO {
    private String brandId;
    private String dealerId;
}
