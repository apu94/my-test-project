package com.ba.gas.monkey.dtos.customer;


import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Cluster;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.models.Thana;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class CustomerBean implements IRequestBodyDTO {
    private String id;
    @NotBlank
    private String customerName;
    @Email
    private String emailAddress;
    @Size(min = 11, max = 16)
    private String phoneNo;
    @ValidEntityId(CustomerType.class)
    private String customerTypeId;
    private String companyName;
    private Boolean status;
    @ValidEntityId(District.class)
    private String districtId;
    @ValidEntityId(Thana.class)
    private String thanaId;
    @ValidEntityId(Cluster.class)
    private String clusterId;
    private String area;
    private String address;
    private String gpsAddress;
    private Double latitude;
    private Double longitude;
    private String floorNo;
    private Boolean liftAllowed;
    @JsonIgnore
    private List<String> authorities;
    @JsonIgnore
    private String password;
    private Integer rewardPoint;
    private String photoLink;
    private String deviceType;
    private String deviceToken;
    private CustomerTypeBean typeBean;
    private String userStatus;
    private Boolean approved;
    private LocalDateTime lastAccess;
}
