package com.ba.gas.monkey.dtos.customer;

import com.ba.gas.monkey.dtos.*;
import com.ba.gas.monkey.dtos.district.DistrictBean;
import com.ba.gas.monkey.dtos.thana.ThanaBean;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class CustomerInfoBean extends RowInfoBean {
    private String id;
    @NotBlank
    private String customerName;
    @Email
    private String emailAddress;
    @Size(min = 11, max = 16)
    private String phoneNo;
    @ValidEntityId(CustomerType.class)
    private String customerTypeId;
    private String companyName;
    private Boolean status;
    private String districtId;
    private String thanaId;
    private String area;
    private String address;
    private String gpsAddress;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    private String floorNo;
    private Boolean liftAllowed;
    private Integer rewardPoint;
    private String photoLink;
    private String userStatus;
    private Boolean approved;
    private DistrictBean district;
    private ThanaBean thana;
    private ClusterBean cluster;
    private LocalDateTime lastAccess;
    private CustomerTypeBean customerTypeBean;
}
