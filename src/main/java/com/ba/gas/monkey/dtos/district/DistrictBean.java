package com.ba.gas.monkey.dtos.district;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class DistrictBean implements IRequestBodyDTO {
    private String id;
    private String name;
    private Boolean status;
}
