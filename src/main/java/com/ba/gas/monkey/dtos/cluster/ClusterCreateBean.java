package com.ba.gas.monkey.dtos.cluster;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Thana;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ClusterCreateBean implements IRequestBodyDTO {
    @NotBlank
    private String name;
    @ValidEntityId(Thana.class)
    private String thanaId;
    private boolean status;
}
