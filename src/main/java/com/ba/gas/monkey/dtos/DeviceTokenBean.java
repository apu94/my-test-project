package com.ba.gas.monkey.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceTokenBean implements IRequestBodyDTO {
    private String userId;
    private String deviceType;
    private String module;
    private String token;
}
