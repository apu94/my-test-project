package com.ba.gas.monkey.dtos;

import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Customer;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDate;

@Data
public class FavouriteBrandBean implements IRequestBodyDTO {
    private String id;
    private Brand brand;
    private Customer customer;
    private Boolean status;
}
