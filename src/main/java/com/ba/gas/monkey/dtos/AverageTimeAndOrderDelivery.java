package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class AverageTimeAndOrderDelivery implements IRequestBodyDTO {
    private Double averageTime;
    private Integer order;
    private Integer delivery;
}
