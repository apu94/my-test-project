package com.ba.gas.monkey.dtos;

import com.ba.gas.monkey.models.AppMenu;
import com.ba.gas.monkey.models.GroupInfo;
import lombok.Data;

@Data
public class RoleMenuBean implements IRequestBodyDTO {
    private GroupInfo groupInfo;
    private AppMenu appMenu;
    private Boolean status;
}
