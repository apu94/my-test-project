package com.ba.gas.monkey.dtos.user;

import lombok.Data;


@Data
public class UserListBean {
    private String id;
    private String userFullName;
    private String email;
    private String mobile;
    private String status;
    private String userType;
    private String designation;
    private String department;
}
