package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class ChangeCartBeanRequest {
    @ValidEntityId(Customer.class)
    private String customerId;
    private Integer floorNo;
    private Boolean isLiftAllowed;
    private Boolean isExpress;
    private Boolean isAdd;
    private String couponCode;
    private String cartId;
}
