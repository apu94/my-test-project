package com.ba.gas.monkey.dtos.dealer;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class DealerDetailsListBean implements IRequestBodyDTO {
    private CustomerInfoBean customerInfoBean;
    @JsonIgnore
    private String customerId;
    private String shopName;
    private String status;
    private String keyContactName;
    private String keyContactPhone;
    private String approved;
    private Double depositBalance;
}
