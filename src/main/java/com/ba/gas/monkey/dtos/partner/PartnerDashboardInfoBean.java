package com.ba.gas.monkey.dtos.partner;

import com.ba.gas.monkey.dtos.PartnerDashboardBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PartnerDashboardInfoBean {
    private PartnerDashboardBean partnerDashboardBean;
    private Double ratingPoint;
    private CustomerBean customerBean;
}
