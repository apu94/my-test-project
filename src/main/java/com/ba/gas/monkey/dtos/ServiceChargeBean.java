package com.ba.gas.monkey.dtos;

import com.ba.gas.monkey.models.ServiceCharge;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ServiceChargeBean implements IRequestBodyDTO {
    @ValidEntityId(ServiceCharge.class)
    private String id;
    @NotBlank
    private String serviceName;
    @NotBlank
    private Double serviceValue;
    @NotNull
    private Boolean status;
}
