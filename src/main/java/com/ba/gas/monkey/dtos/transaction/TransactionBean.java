package com.ba.gas.monkey.dtos.transaction;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TransactionBean implements IRequestBodyDTO {
    @NotNull
    private String note;
    private Double debit;
    private Double credit;
    private Double balance;
    @ValidEntityId(Customer.class)
    private String customerId;
}
