package com.ba.gas.monkey.dtos.thana;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ThanaCreateBean implements IRequestBodyDTO {
    @NotBlank
    private String name;
    @ValidEntityId(District.class)
    private String districtId;
    private Boolean status;
}
