package com.ba.gas.monkey.dtos.brand;

import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FavouriteBrandRequestBean {
    private String id;
    @ValidEntityId(Brand.class)
    private String brandId;
    @ValidEntityId(Customer.class)
    private String customerId;
    @NotNull
    private Boolean status;

}
