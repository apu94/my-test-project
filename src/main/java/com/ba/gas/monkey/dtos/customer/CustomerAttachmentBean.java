package com.ba.gas.monkey.dtos.customer;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.attachment.AttachmentTypeBean;
import com.ba.gas.monkey.models.AttachmentType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CustomerAttachmentBean implements IRequestBodyDTO {
    @ValidEntityId(AttachmentType.class)
    private String attachmentId;
    @NotBlank
    private String customerId;
    private String id;
    private String attachmentLink;
    private AttachmentTypeBean attachmentTypeBean;
}
