package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class TransactionSummaryBean implements IRequestBodyDTO {
    private Double totalEarning;
    private Double totalServiceCharge;
    private Integer numberOfDelivery;
    private String date;
}
