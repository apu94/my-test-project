package com.ba.gas.monkey.dtos.firebase;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

import java.util.Map;

@Data
public class Note implements IRequestBodyDTO {
    private String subject;
    private String content;
    private String type;
    private Map<String, String> dataValue;
}
