package com.ba.gas.monkey.dtos.attachment;

import com.ba.gas.monkey.models.AttachmentType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AttachmentInfoBean {
    @ValidEntityId(AttachmentType.class)
    private String attachmentTypeId;
    @NotBlank
    private String attachmentLink;
}
