package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.OrderedProductBean;
import com.ba.gas.monkey.dtos.customer.CustomerTypeBean;
import com.ba.gas.monkey.projection.OrderHistoryProjectionBean;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
@Builder
public class AdminOrderDetailsBean implements IRequestBodyDTO {
    private String id;
    private String customerId;
    private String customerName;
    private String phoneNo;
    private String address;
    private Long orderNumber;
    private String partnerName;
    private String dealerName;
    private String productDescription;
    private String dateCreated;
    private String orderTime;
    private Integer orderQty;
    private String deliveryMapAddress;
    private String note;
    private String returnProduct;
    private Boolean status;
    private String orderStatus;
    private Double subTotal;
    private Double vat;
    private Double serviceCharge;
    private Double discountAmount;
    private Double total;
    private String couponCode;
    private Double couponValue;
    private Double exchangeRate;
    private Boolean regularDelivery;
    private Boolean isHome;
    private String paymentType;
    private String paymentStatus;
    private List<OrderedProductBean> products;
    private String packagingCharge;
    private String deliveryPersonName;
    private String pickupAddress;
    private List<OrderHistoryInfoBean> orderHistoryBeans;
    private Double totalExchange;
    private Double totalConvenienceFee;
    private String partnerMobileNo;
    private String dealerMobileNo;
    private String dealerAddress;
    private String orderType;
    private Instant orderCreatedDate;

}
