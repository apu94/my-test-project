package com.ba.gas.monkey.dtos.partner;

import com.ba.gas.monkey.dtos.ClusterBean;
import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.customer.CustomerAttachmentBean;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.ba.gas.monkey.dtos.mobilebanking.MobileBankingInfoBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class PartnerDetailsBean implements IRequestBodyDTO {
    private CustomerInfoBean customerInfoBean;
    @JsonIgnore
    private String customerId;
    private String workingZone;
    private String mobileBankId;
    private String mobileWalletNo;
    private String nomineeName;
    private String nomineePhone;
    private String nomineeRelationShip;
    private Boolean status;
    private List<CustomerAttachmentBean> attachmentBeans;
    private MobileBankingInfoBean mobileWallet;
    private ClusterBean workingZoneBean;
    private Double totalEarnings = 0.0;
    private Double totalPayable = 0.0;
}
