package com.ba.gas.monkey.dtos.district;

import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class DistrictListBean extends RowInfoBean {
    private String name;
    private String status;
}
