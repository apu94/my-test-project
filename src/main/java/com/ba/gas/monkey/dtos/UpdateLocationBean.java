package com.ba.gas.monkey.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateLocationBean {
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
}
