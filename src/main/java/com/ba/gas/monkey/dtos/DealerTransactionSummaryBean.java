package com.ba.gas.monkey.dtos;

import com.ba.gas.monkey.dtos.order.OrderBean;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class DealerTransactionSummaryBean {
    private Map<String, List<OrderBean>> orderInfolistGrouped;
    private Integer total;
    private String date;
}
