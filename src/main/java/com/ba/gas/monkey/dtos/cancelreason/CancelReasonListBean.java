package com.ba.gas.monkey.dtos.cancelreason;

import lombok.Data;

@Data
public class CancelReasonListBean {
    private String id;
    private String title;
    private String status;
}
