package com.ba.gas.monkey.dtos.mobilebanking;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MobileBankingInfoListBean implements IRequestBodyDTO {
    private String id;
    @NotBlank
    private String name;
    private String status;
}
