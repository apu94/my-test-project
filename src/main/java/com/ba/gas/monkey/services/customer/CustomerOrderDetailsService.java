package com.ba.gas.monkey.services.customer;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.customer.CustomerOrderInfoBean;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.repositories.OrderRepository;
import com.ba.gas.monkey.services.order.OrderService;
import com.ba.gas.monkey.utility.Constant;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service("customerOrderDetailsService")
public class CustomerOrderDetailsService {

    private final OrderRepository orderRepository;

    public CustomerOrderInfoBean getOrderInfo(String id) {
        List<OrderInfo> infoList = orderRepository.findByCustomerId(id, Sort.by(Sort.Order.desc("dateCreated")));
        List<OrderInfo> completedOrders = infoList.stream().filter(f -> AppConstant.ORDER_COMPLETED_STATUS.contains(f.getOrderStatus().toUpperCase())).collect(Collectors.toUnmodifiableList());
        long cancelledOrders = infoList.stream().filter(f -> f.getOrderStatus().equalsIgnoreCase(Constant.CANCELED)).count();
        long deliveredOrders = infoList.stream().filter(f -> AppConstant.ORDER_COMPLETED_STATUS.contains(f.getOrderStatus().toUpperCase())).count();
        long pendingOrders = infoList.stream().filter(f -> !AppConstant.ORDER_PENDING_STATUS.contains(f.getOrderStatus().toUpperCase())).count();
        double totalPurchase = completedOrders.stream().mapToDouble(OrderInfo::getTotal).sum();
        Instant lastOrder = (infoList.isEmpty()) ? null : infoList.get(0).getDateCreated();
        return CustomerOrderInfoBean.builder()
                .lastOrder(lastOrder)
                .totalPurchase(totalPurchase)
                .pendingOrders(pendingOrders)
                .cancelledOrders(cancelledOrders)
                .totalDelivered(deliveredOrders)
                .totalOrder(infoList.size()).build();
    }

}
