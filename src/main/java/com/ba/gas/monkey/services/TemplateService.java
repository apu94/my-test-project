package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.template.TemplateBean;
import com.ba.gas.monkey.dtos.template.TemplateListBean;
import com.ba.gas.monkey.models.Template;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("templateService")
public class TemplateService extends BaseService<Template, TemplateBean> {

    public TemplateService(BaseRepository<Template> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }


    public Object getTemplateList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("title"));
        Page<Template> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private TemplateListBean getListBean(Template template) {
        TemplateListBean bean = getModelMapper().map(template, TemplateListBean.class);
        bean.setStatus(template.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public List<DropdownDTO> getDropdownList() {
        return getRepository().findAll().stream().filter(Template::getStatus)
                .map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Template template) {
        DropdownDTO dto = getModelMapper().map(template, DropdownDTO.class);
        dto.setValue(template.getTitle());
        return dto;
    }

    public Object save(TemplateBean typeBean) {
        Template template = convertForCreate(typeBean);
        return create(template);
    }

    public Object updateTemplate(String id, TemplateBean typeBean) {
        Template template = getRepository().getById(id);
        BeanUtils.copyProperties(typeBean, template, AppConstant.IGNORE_PROPERTIES);
        return update(id, template);
    }
}
