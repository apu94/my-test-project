package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.sliderImage.SliderImageBean;
import com.ba.gas.monkey.dtos.sliderImage.SliderImageListBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.SliderImage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("sliderImageService")
public class SliderImageService extends BaseService<SliderImage, SliderImageBean> {

    public SliderImageService(BaseRepository<SliderImage> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public Page<SliderImageListBean> getSliderImages(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("status"));
        Page<SliderImage> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    public SliderImageBean createSlider(SliderImageBean bean) {
        SliderImage sliderImage = convertForCreate(bean);
        return create(sliderImage);
    }

    public SliderImageBean updateSlider(String id, SliderImageBean bean) {
        SliderImage sliderImage = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No slider-image found by id:" + id));
        BeanUtils.copyProperties(bean, sliderImage, AppConstant.IGNORE_PROPERTIES);
        return update(id, sliderImage);
    }

    public List<SliderImageListBean> getSliderImagesForApp() {
        List<SliderImage> list = getRepository().findAll(Sort.by("status")).stream().filter(SliderImage::getStatus).collect(Collectors.toUnmodifiableList());
        return list.stream().map(this::getListBean).collect(Collectors.toUnmodifiableList());
    }

    private SliderImageListBean getListBean(SliderImage sliderImage) {
        SliderImageListBean bean = getModelMapper().map(sliderImage, SliderImageListBean.class);
        bean.setStatus(sliderImage.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public String deleteByOid(String id) {
        getRepository().deleteById(id);
        return "delete successful";
    }
}
