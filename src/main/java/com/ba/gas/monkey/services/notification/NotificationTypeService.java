package com.ba.gas.monkey.services.notification;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.notification.NotificationTypeBean;
import com.ba.gas.monkey.models.NotificationType;
import com.ba.gas.monkey.repositories.NotificationTypeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("notificationTypeService")
public class NotificationTypeService extends BaseService<NotificationType, NotificationTypeBean> {
    public NotificationTypeService(BaseRepository<NotificationType> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public Optional<NotificationType> getByType(String type) {
        return ((NotificationTypeRepository) getRepository()).findByName(type);
    }
}
