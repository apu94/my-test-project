package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.ServiceDiscountBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.ServiceDiscount;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("serviceDiscountService")
public class ServiceDiscountService extends BaseService<ServiceDiscount, ServiceDiscountBean> {

    public ServiceDiscountService(BaseRepository<ServiceDiscount> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<ServiceDiscount> getServiceDiscount() {
        return getRepository().findAll();
    }

    public ServiceDiscountBean updateServiceDiscount(String id, ServiceDiscountBean bean) {
        ServiceDiscount serviceDiscount = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No service discount found by id:" + id));
        BeanUtils.copyProperties(bean, serviceDiscount, AppConstant.IGNORE_PROPERTIES);
        return update(id, serviceDiscount);
    }
}
