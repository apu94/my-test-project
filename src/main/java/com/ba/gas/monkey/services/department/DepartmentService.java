package com.ba.gas.monkey.services.department;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.department.DepartmentBean;
import com.ba.gas.monkey.dtos.department.DepartmentListBean;
import com.ba.gas.monkey.dtos.designation.DesignationListBean;
import com.ba.gas.monkey.models.Department;
import com.ba.gas.monkey.models.Template;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Service("departmentService")
public class DepartmentService extends BaseService<Department, DepartmentBean> {

    public DepartmentService(BaseRepository<Department> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        List<Department> list = getRepository().findAll(Sort.by("name")).stream().filter(Department::getStatus).collect(Collectors.toUnmodifiableList());
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(Department department) {
        DropdownDTO dropdownDTO = getModelMapper().map(department, DropdownDTO.class);
        dropdownDTO.setValue(department.getName());
        return dropdownDTO;
    }

    public Object createDepartment(DepartmentBean bean) {
        Department department = convertForCreate(bean);
        return create(department);
    }

    public Object updateDepartment(String id, DepartmentBean bean) {
        Department department = getRepository().getById(id);
        BeanUtils.copyProperties(bean, department, AppConstant.IGNORE_PROPERTIES);
        return update(id, department);
    }

    public Object getDepartmentList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("name"));
        Page<Department> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean)
                .sorted(Comparator.comparing(DepartmentListBean::getStatus))
                .collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private DepartmentListBean getListBean(Department department) {
        DepartmentListBean bean = getModelMapper().map(department, DepartmentListBean.class);
        bean.setStatus(department.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }
}
