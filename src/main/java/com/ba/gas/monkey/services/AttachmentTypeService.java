package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.attachment.AttachmentTypeBean;
import com.ba.gas.monkey.dtos.attachment.AttachmentTypeListBean;
import com.ba.gas.monkey.models.AttachmentType;
import com.ba.gas.monkey.models.Coupon;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("attachmentTypeService")
public class AttachmentTypeService extends BaseService<AttachmentType, AttachmentTypeBean> {

    public AttachmentTypeService(BaseRepository<AttachmentType> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Override
    public Page<AttachmentTypeBean> getList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "status"));
        return super.getList(pageRequest);
    }

    public AttachmentTypeBean createAttachmentType(AttachmentTypeBean bean) {
        return super.create(convertForCreate(bean));
    }

    public List<DropdownDTO> getDropdownList() {
        List<AttachmentType> list = getRepository().findAll().stream().filter(AttachmentType::getStatus).collect(Collectors.toUnmodifiableList());
        return list.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(AttachmentType attachmentType) {
        DropdownDTO dto = getModelMapper().map(attachmentType, DropdownDTO.class);
        dto.setValue(attachmentType.getAttachmentName());
        return dto;
    }

    public AttachmentTypeBean updateAttachmentType(String id, AttachmentTypeBean bean) {
        AttachmentType attachmentType = getById(id);
        BeanUtils.copyProperties(bean, attachmentType, AppConstant.IGNORE_PROPERTIES);
        return update(id, attachmentType);
    }

    public Page<AttachmentTypeListBean> getAttachmentList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "status", "dateCreated"));
        Page<AttachmentType> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getTypeListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private AttachmentTypeListBean getTypeListBean(AttachmentType attachmentType) {
        AttachmentTypeListBean bean = getModelMapper().map(attachmentType, AttachmentTypeListBean.class);
        bean.setStatus(attachmentType.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

}
