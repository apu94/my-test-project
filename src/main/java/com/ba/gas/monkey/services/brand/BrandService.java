package com.ba.gas.monkey.services.brand;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.brand.BrandBean;
import com.ba.gas.monkey.dtos.brand.BrandDetailsBean;
import com.ba.gas.monkey.dtos.brand.BrandImageListBean;
import com.ba.gas.monkey.dtos.brand.BrandListBean;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.BrandImage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("brandService")
public class BrandService extends BaseService<Brand, BrandBean> {

    public BrandService(BaseRepository<Brand> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }


    public Page<BrandListBean> getBrandList(Pageable pageable) {
        Pageable paging = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("status"), Sort.Order.desc("dateModified")));
        Page<BrandBean> page = super.getList(paging);
        return new PageImpl<>(page.getContent().stream().map(this::getBrandListBean)
                .sorted(Comparator.comparing(BrandListBean::getDiscountEnabled).reversed()).collect(Collectors.toUnmodifiableList()),
                page.getPageable(), page.getTotalElements());
    }


    public BrandDetailsBean getBrandDetailsBean(String id) {
        Brand brand = getById(id);
        BrandDetailsBean bean = getModelMapper().map(brand, BrandDetailsBean.class);
        bean.setBrandImages(brand.getBrandImageList().stream().map(this::getImageListBean).collect(Collectors.toUnmodifiableList()));
        bean.setDiscountEnabled(Objects.nonNull(brand.getDiscountEnabled()) && brand.getDiscountEnabled() ? Boolean.TRUE : Boolean.FALSE);
        return bean;
    }

    public List<DropdownDTO> dropdownList() {
        List<Brand> list = getRepository().findAll(Sort.by("nameEn")).stream().filter(Brand::getStatus).collect(Collectors.toUnmodifiableList());
        return list.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Brand brand) {
        DropdownDTO dto = getModelMapper().map(brand, DropdownDTO.class);
        dto.setValue(brand.getNameEn());
        dto.setValueBn(brand.getNameBn());
        return dto;
    }

    public BrandBean createBrand(BrandBean brandBean) {
        String shortCode = (brandBean.getNameEn().length() > 3) ? brandBean.getNameEn().replaceAll("\\s+", "").substring(0, 3).toUpperCase() : brandBean.getNameEn().trim();
        brandBean.setShortName(shortCode);
        return super.create(convertForCreate(brandBean));
    }

    public BrandBean updateBrand(String brandId, BrandBean brandBean) {
        String shortCode = (brandBean.getNameEn().length() > 3) ? brandBean.getNameEn().replaceAll("\\s+", "").substring(0, 3).toUpperCase() : brandBean.getNameEn().trim();
        brandBean.setShortName(shortCode);
        Brand brand = getById(brandId);
        BeanUtils.copyProperties(brandBean, brand, "id", "dateCreated");
        brand.setDiscountEnabled(Objects.isNull(brandBean.getDiscountEnabled()) ? Boolean.FALSE : brandBean.getDiscountEnabled());
        return super.update(brandId, brand);

    }


    private BrandListBean getBrandListBean(BrandBean brand) {
        List<BrandImage> images = brand.getBrandImageList().stream().filter(BrandImage::getStatus).sorted(Comparator.comparing(BrandImage::getDateCreated).reversed()).collect(Collectors.toUnmodifiableList());
        BrandListBean bean = getModelMapper().map(brand, BrandListBean.class);
        bean.setStatus(brand.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        bean.setImageLink(images.isEmpty() ? AppConstant.EMPTY_STRING : images.get(0).getImageLink());
        bean.setDiscountEnabled(Objects.nonNull(brand.getDiscountEnabled()) && brand.getDiscountEnabled() ? AppConstant.STATUS_YES : AppConstant.STATUS_NO);
        return bean;
    }


    private BrandImageListBean getImageListBean(BrandImage brandImage) {
        BrandImageListBean bean = getModelMapper().map(brandImage, BrandImageListBean.class);
        bean.setStatus(brandImage.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public List<BrandBean> getByBrandsId(List<String> brandIds) {
        return convertForRead(getRepository().findAllById(brandIds));
    }
}
