package com.ba.gas.monkey.services.customer;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.CommonResponseBean;
import com.ba.gas.monkey.dtos.attachment.AttachmentInfoBean;
import com.ba.gas.monkey.dtos.attachment.AttachmentTypeBean;
import com.ba.gas.monkey.dtos.customer.CustomerAttachmentBean;
import com.ba.gas.monkey.models.AttachmentType;
import com.ba.gas.monkey.models.CustomerAttachment;
import com.ba.gas.monkey.repositories.CustomerAttachmentRepository;
import com.ba.gas.monkey.services.AttachmentTypeService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("customerAttachmentService")
public class CustomerAttachmentService extends BaseService<CustomerAttachment, CustomerAttachmentBean> {

    private final UserInfoService userInfoService;
    private final AttachmentTypeService attachmentTypeService;

    public CustomerAttachmentService(BaseRepository<CustomerAttachment> repository, ModelMapper modelMapper, UserInfoService userInfoService, AttachmentTypeService attachmentTypeService) {
        super(repository, modelMapper);
        this.userInfoService = userInfoService;
        this.attachmentTypeService = attachmentTypeService;
    }

    public void createCustomerAttachment(String customerId, List<AttachmentInfoBean> attachments) {
        String updateId = userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId();
        List<CustomerAttachment> list = new ArrayList<>();
        attachments.forEach(a -> {
            list.add(getCustomerAttachment(customerId, a));
        });
        list.forEach(l -> {
            l.setUpdtId(updateId);
            l.setDateCreated(Instant.now());
            l.setDateModified(Instant.now());
            l.setStatus(Boolean.TRUE);
        });
        getRepository().saveAll(list);
    }

    private CustomerAttachment getCustomerAttachment(String customerId, AttachmentInfoBean infoBean) {
        CustomerAttachment bean = new CustomerAttachment();
        bean.setAttachmentId(infoBean.getAttachmentTypeId());
        bean.setCustomerId(customerId);
        bean.setAttachmentLink(infoBean.getAttachmentLink());
        bean.setStatus(Boolean.TRUE);
        return bean;
    }

    public List<CustomerAttachmentBean> getAllAttachmentByCustomerId(String id) {
        List<CustomerAttachment> list = ((CustomerAttachmentRepository) getRepository()).findByCustomerId(id);
        List<CustomerAttachmentBean> attachmentBeans = convertForRead(list);
        attachmentBeans.forEach(l -> l.setAttachmentTypeBean(getAttachmentTypeBean(l)));
        return attachmentBeans;
    }

    private AttachmentTypeBean getAttachmentTypeBean(CustomerAttachmentBean bean) {
        return getModelMapper().map(attachmentTypeService.getByOid(bean.getAttachmentId()), AttachmentTypeBean.class);
    }

    public void updateCustomerAttachment(String id, List<AttachmentInfoBean> attachments) {
        List<CustomerAttachment> attachmentList = ((CustomerAttachmentRepository) getRepository()).findByCustomerId(id);
        if (!attachmentList.isEmpty()) {
            getRepository().deleteAll(attachmentList);
        }
        List<CustomerAttachment> list = new ArrayList<>();
        attachments.forEach(a -> {
            list.add(getCustomerAttachment(id, a));
        });
        list.forEach(l -> {
            l.setUpdtId(AuthenticationUtils.getTokenUserId());
            l.setDateCreated(Instant.now());
            l.setDateModified(Instant.now());
            l.setStatus(Boolean.TRUE);
        });
        getRepository().saveAll(list);
    }


    public Object delete(String id) {
        Optional<CustomerAttachment> optional = getRepository().findById(id);
        optional.ifPresent(customerAttachment -> getRepository().delete(customerAttachment));
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }
}
