package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.ServiceChargeBean;
import com.ba.gas.monkey.models.ServiceCharge;
import com.ba.gas.monkey.repositories.ServiceChargeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("serviceChargeService")
public class ServiceChargeService extends BaseService<ServiceCharge, ServiceChargeBean> {

    public ServiceChargeService(BaseRepository<ServiceCharge> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<ServiceChargeBean> getData() {
        return convertForRead(getRepository().findAll());
    }

    public void updateData(List<ServiceChargeBean> charges) {
        for (ServiceChargeBean bean : charges) {
            updateData(bean);
        }
    }

    private void updateData(ServiceChargeBean bean) {
        ServiceCharge serviceCharge = getRepository().getById(bean.getId());
        BeanUtils.copyProperties(bean, serviceCharge, AppConstant.IGNORE_PROPERTIES);
        serviceCharge.setId(bean.getId());
        getRepository().save(serviceCharge);
    }

    public Optional<ServiceCharge> getByServiceType(String serviceName) {
        return ((ServiceChargeRepository)getRepository()).findByServiceName(serviceName);
    }
}
