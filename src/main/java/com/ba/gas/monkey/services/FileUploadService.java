package com.ba.gas.monkey.services;

import com.ba.gas.monkey.dtos.FileUploadResponseBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service("fileUploadService")
public class FileUploadService {

    @Value("${api.server.url}")
    private String location;
    @Value("${upload.folder}")
    private String uploadFolder;

    public FileUploadResponseBean saveFile(MultipartFile file) {
        boolean success = false;
        String path = null;
        try {
            String ext = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("."));
            String destination = UUID.randomUUID() + ext;
            System.out.println(destination);
            String filePath = uploadFolder + destination;
            File fileToSave = new File(filePath);
            file.transferTo(fileToSave);
            path = location + destination;
            success = true;
        } catch (Exception exception) {
            log.error("File-upload-exception =====>>> {}", exception.getMessage());
        }
        return FileUploadResponseBean.builder().success(success).path(path).build();
    }
}
