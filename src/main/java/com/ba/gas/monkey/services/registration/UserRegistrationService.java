package com.ba.gas.monkey.services.registration;

import com.ba.gas.monkey.dtos.user.UserRegistrationBean;

public interface UserRegistrationService {

    void userRegistration(UserRegistrationBean registrationDTO);

}
