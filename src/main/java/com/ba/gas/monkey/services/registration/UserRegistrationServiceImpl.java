package com.ba.gas.monkey.services.registration;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.user.UserRegistrationBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.UserInfo;
import com.ba.gas.monkey.repositories.DepartmentRepository;
import com.ba.gas.monkey.repositories.GroupInfoRepository;
import com.ba.gas.monkey.repositories.UserInfoRepository;
import com.ba.gas.monkey.services.department.DepartmentService;
import com.ba.gas.monkey.services.designation.DesignationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service("userRegistrationService")
class UserRegistrationServiceImpl implements UserRegistrationService {

    private final PasswordEncoder passwordEncoder;
    private final UserInfoRepository userInfoRepository;
    private final GroupInfoRepository groupInfoRepository;
    private final DepartmentService departmentService;
    private final DesignationService designationService;
    private final ModelMapper modelMapper;

    @Override
    public void userRegistration(UserRegistrationBean userRegistrationBean) {

        log.info("user-registration dto {}", userRegistrationBean);
        String password = userRegistrationBean.getPassword();
        String confirmPassword = userRegistrationBean.getConfirmPassword();
        if (!password.equals(confirmPassword)) {
            throw new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.PASSWORD_MISMATCH_EXCEPTION_CODE, "please check password");
        }
        Optional<UserInfo> optionalUserInfo = userInfoRepository.findByEmailAddress(userRegistrationBean.getEmail());
        if (optionalUserInfo.isPresent()) {
            throw new ServiceExceptionHolder.CustomException(ServiceExceptionHolder.PASSWORD_MISMATCH_EXCEPTION_CODE, userRegistrationBean.getEmail() + " : email is already in use");
        }
        log.info("password {}", passwordEncoder.encode(password));
        UserInfo userInfo = getUserInfo(userRegistrationBean);
        userInfo.setStatus(AppConstant.ACTIVE_USER);
        userInfo.setPassword(passwordEncoder.encode(password));
        userInfo.setDateCreated(Instant.now());
        userInfo.setDateModified(Instant.now());
        userInfo.setGroupInfo(groupInfoRepository.getById(userRegistrationBean.getGroupId()));
        userInfo.setDepartment(departmentService.getRepository().getById(userRegistrationBean.getDepartmentId()));
        userInfo.setDesignation(designationService.getRepository().getById(userRegistrationBean.getDesignationId()));
        userInfo.setUpdtId(userInfoRepository.findByEmailAddress(AppConstant.SUPER_ADMIN_EMAIL).get().getId());
        userInfoRepository.save(userInfo);
    }

    private UserInfo getUserInfo(UserRegistrationBean registrationDTO) {
        return modelMapper.map(registrationDTO, UserInfo.class);
    }
}
