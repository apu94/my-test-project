package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.CommonResponseBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.district.DistrictListBean;
import com.ba.gas.monkey.dtos.product.ProductSizeBean;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.models.ProductSize;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("productSizeService")
public class ProductSizeService extends BaseService<ProductSize, ProductSizeBean> {
    public ProductSizeService(BaseRepository<ProductSize> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public Page<ProductSizeListBean> getProductSizeList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("dateCreated")));
        Page<ProductSize> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent()
                .stream()
                .map(this::getProductSizeListBean)
                .sorted(Comparator.comparing(ProductSizeListBean::getStatus))
                .collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private ProductSizeListBean getProductSizeListBean(ProductSize productSize) {
        ProductSizeListBean bean = getModelMapper().map(productSize, ProductSizeListBean.class);
        bean.setStatus(productSize.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }


    public List<DropdownDTO> getDropdownList() {
        List<ProductSize> list = getRepository().findAll(Sort.by("nameEn"));
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(ProductSize Size) {
        DropdownDTO dropdownDTO = getModelMapper().map(Size, DropdownDTO.class);
        dropdownDTO.setValue(Size.getNameEn());
        dropdownDTO.setValueBn(Size.getNameBn());
        return dropdownDTO;
    }

    public Object createProductSize(ProductSizeBean bean) {
        ProductSize productSize = convertForCreate(bean);
        return create(productSize);
    }

    public Object deleteProductSize(String id) {
        ProductSize productSize = getRepository().getById(id);
        productSize.setStatus(Boolean.FALSE);
        update(id,productSize);
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public Object updateProductSize(String id, ProductSizeBean bean) {
        ProductSize productSize = getRepository().getById(id);
        BeanUtils.copyProperties(bean,productSize,AppConstant.IGNORE_PROPERTIES);
        return update(id,productSize);

    }
}
