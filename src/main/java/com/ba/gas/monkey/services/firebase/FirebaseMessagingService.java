package com.ba.gas.monkey.services.firebase;

import com.ba.gas.monkey.dtos.firebase.Note;
import com.ba.gas.monkey.utility.Constant;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.*;
import org.springframework.stereotype.Service;

@Service
public class FirebaseMessagingService {

    private final ObjectMapper objectMapper;
    private final FirebaseMessaging firebaseMessaging;

    public FirebaseMessagingService(FirebaseMessaging firebaseMessaging, ObjectMapper objectMapper) {
        this.firebaseMessaging = firebaseMessaging;
        this.objectMapper = objectMapper;
    }
    public String sendNotificationAndroid(Note note, String token) throws FirebaseMessagingException {
        System.out.println("inside android notification");
        Notification notification = Notification
                .builder()
                .setTitle(note.getSubject())
                .setBody(note.getContent())
                .build();
        note.getDataValue().put("title",note.getSubject());
        note.getDataValue().put("body",note.getContent());
//        Map<String, String> map = objectMapper.convertValue(note.getDataValue(), Map.class);
        Message message = null;

        if(note.getType() != null && note.getType().equalsIgnoreCase(Constant.PARTNER)){
            message = Message
                    .builder()
                    .setToken(token)
                    .setNotification(null)
                    .putAllData(note.getDataValue())
                    .build();
        }else if(note.getType() == null || note.getType().equalsIgnoreCase(Constant.DEALER)){
            message = Message
                    .builder()
                    .setToken(token)
                    .setNotification(null)
                    .putAllData(note.getDataValue())
                    .build();
        }else if(note.getType() == null || note.getType().equalsIgnoreCase(Constant.CUSTOMER)){
            message = Message
                    .builder()
                    .setToken(token)
                    .setNotification(null)
                    .putAllData(note.getDataValue())
                    .build();
        }
        return firebaseMessaging.send(message);
    }
    public String sendNotification(Note note, String token) throws FirebaseMessagingException {

        Notification notification = Notification
                .builder()
                .setTitle(note.getSubject())
                .setBody(note.getContent())
                .build();
//        note.getDataValue().put("title",note.getSubject());
//        note.getDataValue().put("body",note.getContent());
//        Map<String, String> map = objectMapper.convertValue(note.getDataValue(), Map.class);
        Message message = null;

        if(note.getType().equalsIgnoreCase(Constant.PARTNER)){
            message = Message
                    .builder()
                    .setToken(token)
                    .setNotification(notification)
                    .putAllData(note.getDataValue())
                    .setAndroidConfig(AndroidConfig.builder()
                            .setNotification(AndroidNotification.builder()
                                    .setSound("custom_notification")
                                    .build())
                            .build())
                    .setApnsConfig(ApnsConfig.builder()
                            .setAps(Aps.builder()
                                    .setBadge(1)
                                    .setSound("custom_notification.mp3")
                                    .build())
                            .build())
                    .build();
        }else if(note.getType().equalsIgnoreCase(Constant.DEALER)){
            message = Message
                    .builder()
                    .setToken(token)
                    .setNotification(notification)
                    .putAllData(note.getDataValue())
                    .setApnsConfig(ApnsConfig.builder()
                            .setAps(Aps.builder()
                                    .setBadge(1)
                                    .setSound("default")
                                    .build())
                            .build())
                    .build();
        }else if(note.getType().equalsIgnoreCase(Constant.CUSTOMER)){
            message = Message
                    .builder()
                    .setToken(token)
                    .setNotification(notification)
                    .putAllData(note.getDataValue())
                    .setApnsConfig(ApnsConfig.builder()
                            .setAps(Aps.builder()
                                    .setBadge(1)
                                    .setSound("default")
                                    .build())
                            .build())
                    .build();
        }
        return firebaseMessaging.send(message);
    }

}
