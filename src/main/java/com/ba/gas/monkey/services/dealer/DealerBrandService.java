package com.ba.gas.monkey.services.dealer;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.brand.BrandBean;
import com.ba.gas.monkey.dtos.dealer.DealerBrandBean;
import com.ba.gas.monkey.dtos.dealer.DealerBrandListBean;
import com.ba.gas.monkey.dtos.dealer.DealerDetailsBean;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.DealerBrand;
import com.ba.gas.monkey.repositories.DealerBrandRepository;
import com.ba.gas.monkey.services.brand.BrandService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("dealerBrandService")
public class DealerBrandService extends BaseService<DealerBrand, DealerBrandBean> {

    private final UserInfoService userInfoService;
    private final BrandService brandService;

    public DealerBrandService(BaseRepository<DealerBrand> repository, ModelMapper modelMapper,
                              UserInfoService userInfoService, BrandService brandService) {
        super(repository, modelMapper);
        this.userInfoService = userInfoService;
        this.brandService = brandService;
    }


    private DealerBrand getDealerBrand(String dealerId, String brandId) {
        DealerBrand bean = new DealerBrand();
        bean.setDealerId(dealerId);
        bean.setBrandId(brandId);
        return bean;
    }

    public void createDealerBrand(DealerDetailsBean detailsBean, List<String> list) {
        String updateId = userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId();
        List<DealerBrand> dealerBrands = new ArrayList<>();
        for (String brandId : list) {
            dealerBrands.add(getDealerBrand(detailsBean.getCustomerId(), brandId));
        }
        dealerBrands.forEach(b -> {
            b.setUpdtId(updateId);
            b.setDateCreated(Instant.now());
            b.setDateModified(Instant.now());
            b.setStatus(Boolean.TRUE);
        });
        getRepository().saveAll(dealerBrands);
    }

    public List<BrandBean> getByDealerId(String id) {
        List<DealerBrand> list = ((DealerBrandRepository) getRepository()).findByDealerId(id);
        List<String> brandIds = list.stream().map(DealerBrand::getBrandId).collect(Collectors.toUnmodifiableList());
        return brandService.getByBrandsId(brandIds);
    }

    public void updateDealerBrands(String id, List<String> brandIds) {
        List<DealerBrand> list = ((DealerBrandRepository) getRepository()).findByDealerId(id);
        if (!list.isEmpty()) {
            getRepository().deleteAll(list);
        }
        List<DealerBrand> dealerBrands = new ArrayList<>();
        for (String brandId : brandIds) {
            dealerBrands.add(getDealerBrand(id, brandId));
        }
        dealerBrands.forEach(b -> {
            b.setUpdtId(AuthenticationUtils.getTokenUserId());
            b.setDateCreated(Instant.now());
            b.setDateModified(Instant.now());
            b.setStatus(Boolean.TRUE);
        });
        getRepository().saveAll(dealerBrands);
    }
}
