package com.ba.gas.monkey.services;

import com.ba.gas.monkey.dtos.DealerDashboardBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.order.OrderService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@AllArgsConstructor
@Service("dealerDashboardService")
public class DealerDashboardService {

    private final CustomerService customerService;
    private final OrderService orderService;

    public DealerDashboardBean dashboardData(String date) {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        DealerDashboardBean dealerDashboard = orderService.getDealerDashboard(customer.getId(), date);
        dealerDashboard.setCustomerBean(customerService.customerDetails(customer.getId()));
        return dealerDashboard;
    }
}
