package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.group.GroupInfoBean;
import com.ba.gas.monkey.models.GroupInfo;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("groupInfoService")
public class GroupInfoService extends BaseService<GroupInfo, GroupInfoBean> {

    public GroupInfoService(BaseRepository<GroupInfo> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        List<GroupInfo> groupInfos = getRepository().findAll();
        return groupInfos.stream().filter(l -> !l.getGroupType().equalsIgnoreCase(AppConstant.SUPER_ADMIN)).map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(GroupInfo groupInfo) {
        DropdownDTO dropdownDTO = getModelMapper().map(groupInfo, DropdownDTO.class);
        dropdownDTO.setValue(groupInfo.getGroupName());
        return dropdownDTO;
    }
}
