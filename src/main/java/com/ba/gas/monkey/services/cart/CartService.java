package com.ba.gas.monkey.services.cart;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.cart.*;
import com.ba.gas.monkey.dtos.order.OrderBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.*;
import com.ba.gas.monkey.services.ServiceChargeService;
import com.ba.gas.monkey.services.ServiceDiscountService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.product.ProductService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.Constant;
import com.ba.gas.monkey.utility.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service("cartService")
public class CartService extends BaseService<Cart, CartBean> {

    private final CartRepository cartRepository;
    private final CartDetailService cartDetailService;
    private final UserInfoService userInfoService;
    private final CustomerService customerService;
    private final ProductService productService;
    private final ServiceChargeService serviceChargeService;
    private final ServiceDiscountService serviceDiscountService;
    private final OrderRepository orderRepository;
    private final CouponRepository couponRepository;


    public CartService(BaseRepository<Cart> repository, ModelMapper modelMapper, CartRepository cartRepository,
                       CartDetailService cartDetailService, UserInfoService userInfoService,
                       CustomerService customerService, ProductService productService,
                       ServiceChargeService serviceChargeService, ServiceDiscountService serviceDiscountService,
                       OrderRepository orderRepository,
                       CouponRepository couponRepository) {
        super(repository, modelMapper);
        this.cartRepository = cartRepository;
        this.cartDetailService = cartDetailService;
        this.userInfoService = userInfoService;
        this.customerService = customerService;
        this.productService = productService;
        this.serviceChargeService = serviceChargeService;
        this.serviceDiscountService = serviceDiscountService;
        this.orderRepository = orderRepository;
        this.couponRepository = couponRepository;
    }

    //    @Transactional
    public CartBean addToCart(AddCartBeanRequest addCartBeanRequest) {
        Cart cart = new Cart();
        Optional<Cart> cartOptional = cartRepository.findByCustomerId(addCartBeanRequest.getCustomerId());
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
        } else {
            cart.setCustomer(customerService.getRepository().getById(addCartBeanRequest.getCustomerId()));
            cart.setStatus(true);
            cart.setUpdtId(userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId());
            cart = cartRepository.save(cart);
        }
        Cart finalCart = cart;
        if (cartOptional.get().getCartDetailList().size() + addCartBeanRequest.getCartDetailBeanRequest().getQuantity() > 2) {
            throw new ServiceExceptionHolder.IdNotFoundInDBException("You can add maximum two cylinders in a single order.");
        } else {
            for (int i = 0; i < addCartBeanRequest.getCartDetailBeanRequest().getQuantity(); i++) {
                CartDetail cartDetail = getModelMapper().map(addCartBeanRequest.getCartDetailBeanRequest(), CartDetail.class);
                cartDetail.setBuyProduct(productService.getRepository().findById(addCartBeanRequest.getCartDetailBeanRequest().getBuyProduct()).get());
                if (addCartBeanRequest.getCartDetailBeanRequest().getReturnProduct() != null) {
                    cartDetail.setReturnProduct(productService.getRepository().findById(addCartBeanRequest.getCartDetailBeanRequest().getReturnProduct()).get());
                }
                cartDetail.setQuantity(1);
                cartDetail.setCart(finalCart);
                cartDetail.setUpdtId(userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId());
                cartDetailService.getRepository().save(cartDetail);
            }
        }
        return getModelMapper().map(cartRepository.getById(cart.getId()), CartBean.class);
    }

    public CartBean deleteFromCart(DeleteCartBeanRequest deleteCartBeanRequest) {
        cartDetailService.getRepository().deleteById(deleteCartBeanRequest.getCartDetailId());
        Cart cart;
        Optional<Cart> cartOptional = cartRepository.findByCustomerId(deleteCartBeanRequest.getCustomerId());
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
            return getModelMapper().map(cart, CartBean.class);
        }
        return null;
    }

    public GetCartResponseBean getCart(String id) {
        Customer customer = customerService.getRepository().findById(id).get();
        Optional<Cart> cart = cartRepository.findByCustomerId(customer.getId());
        GetCartResponseBean responseBean = getCartResponseBean(cart.get());
        log.info("cart-customer-info ==========>> {}", customer);
        log.info("cart-responseBean ==========>> {}", responseBean);
        return responseBean;
    }

    public GetCartResponseBean getCartResponseBean(Cart cart) {
        Double serviceCharge = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("SERVICE_CHARGE").get().getServiceValue();

        GetCartResponseBean responseBean = new GetCartResponseBean();
        cart.getCartDetailList().forEach(cartDetail -> {
            Double calculateBuyPrice = cartDetail.getReturnProduct() != null ? cartDetail.getBuyProduct().getProductPrice().getRefillPrice() : cartDetail.getBuyProduct().getProductPrice().getPackagePrice();
            log.info("calculate-price======>>>>{}", calculateBuyPrice);
            calculateBuyPrice = calculateBuyPrice * cartDetail.getQuantity();
            log.info("calculateBuyPrice * cartDetail.getQuantity()======>>>>{}", calculateBuyPrice);
            responseBean.setSubTotal(responseBean.getSubTotal() + calculateBuyPrice);
            responseBean.setDiscountAmount(responseBean.getDiscountAmount() + getDiscountAmount(cartDetail));
            responseBean.setTotalConvenienceFee(responseBean.getTotalConvenienceFee() + cartDetail.getQuantity() * cartDetail.getBuyProduct().getConvenienceFee());
            if (cartDetail.getReturnProduct() != null) {
                Double exchange = cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice() - cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice();
                if (!cartDetail.getBuyProduct().getBrand().getId().equalsIgnoreCase(cartDetail.getReturnProduct().getBrand().getId()) && exchange > 0) {
                    responseBean.setTotalExchange(responseBean.getTotalExchange() + exchange);
                } else if (!cartDetail.getBuyProduct().getBrand().getId().equalsIgnoreCase(cartDetail.getReturnProduct().getBrand().getId()) && exchange == 0) {
                    responseBean.setTotalExchange(responseBean.getTotalExchange() + getExchangeRate());
                }
            }
        });
        responseBean.setServiceCharge((serviceCharge - calculateServiceChargeDiscount(serviceCharge)) * cart.getCartDetailList().size());
        responseBean.setVat(calculateVat(responseBean));
        responseBean.setTotal(calculateTotal(responseBean));
        responseBean.setStatus(cart.getStatus());
        responseBean.setCustomer(cart.getCustomer());
        responseBean.setId(cart.getId());
        responseBean.setCartDetailList(cart.getCartDetailList());
        Double vatPercentage = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("VAT_RATE").get().getServiceValue();
        responseBean.setVatPercent(vatPercentage);
        Double exchangeRate = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("EXCHANGE_RATE").get().getServiceValue();
        responseBean.setExchange(exchangeRate);
        Double perFloorCharge = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("LIFTING_CHARGE").get().getServiceValue();
        responseBean.setPerFloorCharge(perFloorCharge);
        Double floorNoCharge = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("FLOOR_NO_CHARGE").get().getServiceValue();
        responseBean.setFloorNoCharge(floorNoCharge);
        Double expressServiceCharge = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("EXPRESS_SERVICE_CHARGE").get().getServiceValue();
        responseBean.setExpressServiceCharge((expressServiceCharge - calculateServiceChargeDiscount(expressServiceCharge)) * cart.getCartDetailList().size());
        return responseBean;
    }

    public double calculatePrice(CartDetail cartDetail) {
        return cartDetail.getReturnProduct() != null ? cartDetail.getBuyProduct().getProductPrice().getRefillPrice() : cartDetail.getBuyProduct().getProductPrice().getPackagePrice();
    }

    public double getDiscountAmount(CartDetail cartDetail) {

        boolean checkOfferValidation = checkOfferValidation(cartDetail.getBuyProduct().getBrand());
        if (!checkOfferValidation) {
            return 0.0;
        }
        boolean dateChecked = DateUtils.isDateInBetweenIncludingEndPoints(cartDetail.getBuyProduct().getBrand().getDiscountStartDate(), cartDetail.getBuyProduct().getBrand().getDiscountEndDate(), LocalDateTime.now());
        log.info("brand-discount-date-checked {}", dateChecked);
        if (cartDetail.getBuyProduct().getBrand().getDiscountEnabled() && dateChecked) {
            log.info("start-checking");
            if (cartDetail.getBuyProduct().getBrand().getDiscountType().equalsIgnoreCase("PERCENT")) {
                double buyPrice = calculatePrice(cartDetail);
                log.info("buyPrice========={}", buyPrice);
                log.info("percent-discount-value =====>>>>> {}", (cartDetail.getBuyProduct().getBrand().getDiscountValue() * buyPrice) / 100);
                return (cartDetail.getBuyProduct().getBrand().getDiscountValue() * buyPrice) / 100;
            } else {
                log.info("discount-value =====>>>>> {}", (cartDetail.getBuyProduct().getBrand().getDiscountValue()));
                return cartDetail.getBuyProduct().getBrand().getDiscountValue();
            }
        }
        return 0.0;
    }

    private boolean checkOfferValidation(Brand brand) {
        if (Objects.nonNull(brand.getDiscountEnabled()) && Objects.nonNull(brand.getDiscountEndDate()) && brand.getDiscountEnabled()) {
            return brand.getDiscountEndDate().isAfter(LocalDate.now());
        }
        return Boolean.FALSE;
    }

    public double calculateVat(GetCartResponseBean responseBean) {
        Double vatPercentage = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("VAT_RATE").get().getServiceValue();
        // change
        //        double totalAmount = responseBean.getSubTotal() + responseBean.getDiscountAmount();
        double totalAmount = responseBean.getSubTotal();
        return (vatPercentage * totalAmount) / 100;
    }

    public double calculateTotal(GetCartResponseBean responseBean) {
        log.info("GetCartResponseBean========={}", responseBean);
        return (responseBean.getSubTotal() + responseBean.getTotalExchange() + responseBean.getTotalConvenienceFee() - responseBean.getDiscountAmount());
    }

    public double calculateServiceChargeDiscount(double serviceCharge) {
        if (serviceDiscountService.getServiceDiscount().get(0).getDiscountEnable() && DateUtils.isDateInBetweenIncludingEndPoints(serviceDiscountService.getServiceDiscount().get(0).getDiscountStartDate(), serviceDiscountService.getServiceDiscount().get(0).getDiscountEndDate(), LocalDateTime.now())) {
            if (serviceDiscountService.getServiceDiscount().get(0).getDiscountType().equalsIgnoreCase("PERCENT")) {
                return (serviceDiscountService.getServiceDiscount().get(0).getDiscountValue() * serviceCharge) / 100;
            } else {
                return serviceDiscountService.getServiceDiscount().get(0).getDiscountValue();
            }
        }
        return 0.0;
    }

    public double getExchangeRate() {
        return ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("EXCHANGE_RATE").get().getServiceValue();
    }

    public GetCartResponseBean repeatOrderGetCart(String orderId) {
        OrderBean bean = getModelMapper().map(orderRepository.findById(orderId).get(), OrderBean.class);
        Cart cart = cartRepository.findByCustomerId(bean.getCustomer().getId()).get();
        List<CartDetail> details = ((CartDetailRepository) cartDetailService.getRepository()).findByCart(cart);
        cartDetailService.getRepository().deleteAll(details);
//        cart.getCartDetailList().forEach(cartDetail -> {
//            DeleteCartBeanRequest deleteCartBeanRequest = new DeleteCartBeanRequest();
//            deleteCartBeanRequest.setCustomerId(bean.getCustomer().getId());
//            deleteCartBeanRequest.setCartDetailId(cartDetail.getId());
//            deleteFromCart(deleteCartBeanRequest);
//        });
        bean.getOrderProductList().forEach(orderProduct -> {
            AddCartBeanRequest addCartBeanRequest = new AddCartBeanRequest();
            addCartBeanRequest.setCustomerId(bean.getCustomer().getId());
            CartDetailBeanRequest cartDetailBeanRequest = new CartDetailBeanRequest();
            cartDetailBeanRequest.setBuyProduct(orderProduct.getBuyProduct().getId());
            if (orderProduct.getReturnProduct() != null) {
                cartDetailBeanRequest.setReturnProduct(orderProduct.getReturnProduct().getId());
            }
            cartDetailBeanRequest.setQuantity(orderProduct.getQuantity());
            addCartBeanRequest.setCartDetailBeanRequest(cartDetailBeanRequest);
            addToCart(addCartBeanRequest);
        });
        return getCart(bean.getCustomer().getId());
    }

    public GetCartResponseBean changeCart(ChangeCartBeanRequest bean) {
        Customer customer = customerService.getRepository().findById(bean.getCustomerId()).get();
        Optional<Cart> cart = cartRepository.findByCustomerId(customer.getId());
        GetCartResponseBean responseBean = getCartResponseBean(cart.get());
        log.info("cart-customer-info ==========>> {}", customer);
        log.info("cart-responseBean ==========>> {}", responseBean);
        return updateCart(bean, responseBean);
    }

    public GetCartResponseBean updateCart(ChangeCartBeanRequest bean, GetCartResponseBean responseBean) {
        //express
        if (Boolean.TRUE.equals(bean.getIsExpress())) {
            responseBean.setServiceCharge(responseBean.getExpressServiceCharge());
            responseBean.setTotal(responseBean.getTotal() + responseBean.getExpressServiceCharge());
        } else {
            responseBean.setServiceCharge(responseBean.getServiceCharge());
            responseBean.setTotal(responseBean.getTotal() + responseBean.getServiceCharge());
        }

        //is lift allowed and floor
        responseBean = calculateTotalAndServiceChargeForLiftAndFloor(bean, responseBean);

        //coupon
        if (bean.getCouponCode() != null && !bean.getCouponCode().isEmpty()) {
            Coupon coupon = couponRepository.findByCouponCode(bean.getCouponCode()).get();
            responseBean = addCouponFunc(coupon, responseBean);
        }
        responseBean.setCouponCode(bean.getCouponCode());
        return responseBean;
    }

    public GetCartResponseBean couponAddRemove(ChangeCartBeanRequest bean) {
        Customer customer = customerService.getRepository().findById(bean.getCustomerId()).get();
        Optional<Cart> cart = cartRepository.findByCustomerId(customer.getId());
        GetCartResponseBean responseBean = getCartResponseBean(cart.get());
        if (Boolean.TRUE.equals(bean.getIsAdd())) {
            if (bean.getCouponCode() == null || bean.getCouponCode().isEmpty()) {
                throw new ServiceExceptionHolder.IdNotFoundInDBException("Not valid coupon! code:" + bean.getCouponCode());
            }
            Coupon coupon = couponRepository.findByCouponCode(bean.getCouponCode()).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No coupon found by code:" + bean.getCouponCode()));
            if (Objects.equals(coupon.getUsageLimit(), coupon.getCouponUsed())) {
                throw new ServiceExceptionHolder.IdNotFoundInDBException("Coupon already used! code:" + bean.getCouponCode());
            }
            if (!DateUtils.isDateInBetweenIncludingEndPoints(coupon.getStartDate(), coupon.getEndDate(), LocalDateTime.now())) {
                throw new ServiceExceptionHolder.IdNotFoundInDBException("Coupon already expired! code:" + bean.getCouponCode());
            }
        }
        return updateCart(bean, responseBean);
    }

    public GetCartResponseBean calculateTotalAndServiceChargeForLiftAndFloor(ChangeCartBeanRequest bean, GetCartResponseBean responseBean) {

        if (Boolean.FALSE.equals(bean.getIsLiftAllowed()) && isNeedCal(bean, responseBean)) {
            int selectedFloorNumber = bean.getFloorNo();
            int countableFloorNumber = selectedFloorNumber - responseBean.getFloorNoCharge().intValue();
            Double chargeAmount = responseBean.getPerFloorCharge() * countableFloorNumber;
            Double serviceCharge = responseBean.getServiceCharge() + chargeAmount;
            Double total = responseBean.getTotal() + chargeAmount;
            responseBean.setServiceCharge(serviceCharge);
            responseBean.setTotal(total);
        }
        return responseBean;
    }

    private GetCartResponseBean addCouponFunc(Coupon coupon, GetCartResponseBean cartResponse) {
        String couponType = coupon.getCouponType();
        String couponUsageType = coupon.getCouponUsageType();
        if (Objects.nonNull(cartResponse)) {
//            double orderPrice = cartResponse.getSubTotal() + cartResponse.getDiscountAmount(); //it's required when coupon use type Order;
            double orderPrice = cartResponse.getSubTotal();
            double servicePrice = cartResponse.getServiceCharge(); //it's required when coupon use type Service;

            double total = cartResponse.getTotal(); //it's require for both
            double subtotal = cartResponse.getTotal(); //it's require for both
            Double amount = coupon.getAmount();
            if (couponUsageType.equals(Constant.COUPON_USE_TYPE_SERVICE)) { //SERVICE
                cartResponse = getCalculatingAmount(total, servicePrice, amount, couponType, cartResponse, subtotal);
            } else if (couponUsageType.equals(Constant.COUPON_USE_TYPE_ORDER)) { //ORDER
                cartResponse = getCalculatingAmount(total, orderPrice, amount, couponType, cartResponse, subtotal);
            }
        }
        return cartResponse;
    }

    private GetCartResponseBean getCalculatingAmount(Double total, Double tpPrice, Double amount, String couponType, GetCartResponseBean cartResponse, Double subtotal) {
        if (couponType.equals(Constant.DISCOUNT_TYPE_AMOUNT)) //AMOUNT
        {
            cartResponse.setCouponAmount(amount);
            cartResponse.setTotal(total - amount);

        } else if (couponType.equals(Constant.DISCOUNT_TYPE_PERCENT))//PERCENT
        {
            Double couponValue = getDiscountPriceFromPercentage(tpPrice, amount.intValue());
            cartResponse.setCouponAmount(couponValue);
            cartResponse.setTotal(total - couponValue);

        }
        return cartResponse;
    }


    public static Double getDiscountPriceFromPercentage(Double unitPrice, Integer percentage) {
        return (unitPrice * percentage) / 100;
    }

    private static boolean isNeedCal(ChangeCartBeanRequest bean, GetCartResponseBean responseBean) {
        int value = responseBean.getFloorNoCharge().intValue();
        return (bean.getFloorNo() > value);
    }
}
