package com.ba.gas.monkey.services.dealer;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.dtos.customer.CustomerTypeBean;
import com.ba.gas.monkey.dtos.dealer.DealerCreateBean;
import com.ba.gas.monkey.dtos.dealer.DealerDetailsBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.models.DealerDetails;
import com.ba.gas.monkey.services.customer.CustomerAttachmentService;
import com.ba.gas.monkey.services.customer.CustomerRegistrationService;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@AllArgsConstructor
@Service("dealerRegistrationService")
public class DealerRegistrationService {

    private final ModelMapper modelMapper;
    private final CustomerRegistrationService customerRegistrationService;
    private final DealerDetailsService dealerDetailsService;
    private final CustomerTypeService customerTypeService;

    private final DealerBrandService dealerBrandService;
    private final CustomerAttachmentService customerAttachmentService;

    @Transactional
    public DealerDetailsBean createDealerRegistration(DealerCreateBean bean) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.DEALER_TYPE);
        bean.setCustomerTypeId(customerType.getId());
        CustomerRegistrationBean customerRegistrationBean = modelMapper.map(bean, CustomerRegistrationBean.class);
        customerRegistrationBean.setApproved(Boolean.FALSE);
        customerRegistrationBean.setUserStatus(AppConstant.INACTIVE.toLowerCase());
        CustomerInfoBean customerRegistration = customerRegistrationService.createCustomerRegistration(customerRegistrationBean);

        DealerDetailsBean dealerDetails = modelMapper.map(bean, DealerDetailsBean.class);
        dealerDetails.setCustomerId(customerRegistration.getId());
        dealerDetails.setStatus(customerRegistration.getStatus());
        DealerDetails createdDealBean = dealerDetailsService.saveDealDetails(dealerDetails);
        DealerDetailsBean detailsBean = modelMapper.map(createdDealBean, DealerDetailsBean.class);
        detailsBean.setCustomerInfoBean(customerRegistration);
        if (Objects.nonNull(bean.getBrandIds())) {
            dealerBrandService.createDealerBrand(detailsBean, bean.getBrandIds());
        }
        if (Objects.nonNull(bean.getAttachments())) {
            customerAttachmentService.createCustomerAttachment(detailsBean.getCustomerId(), bean.getAttachments());
        }


        return detailsBean;
    }


}
