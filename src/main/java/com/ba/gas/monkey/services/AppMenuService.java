package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.appmenu.AppMenuBean;
import com.ba.gas.monkey.models.AppMenu;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("appMenuService")
public class AppMenuService extends BaseService<AppMenu, AppMenuBean> {
    public AppMenuService(BaseRepository<AppMenu> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<AppMenuBean> getMenuList() {
        List<AppMenu> menus = getRepository().findAll().stream()
                .filter(AppMenu::getStatus).sorted(Comparator.comparing(AppMenu::getTitle)).collect(Collectors.toUnmodifiableList());
        return convertForRead(menus);
    }
}
