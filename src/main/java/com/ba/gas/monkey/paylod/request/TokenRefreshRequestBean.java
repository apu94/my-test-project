package com.ba.gas.monkey.paylod.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TokenRefreshRequestBean {
    @NotBlank
    private String refreshToken;
}
