package com.ba.gas.monkey.paylod.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LogoutResponseBean {
    private Boolean success;
    private String msg;
}
