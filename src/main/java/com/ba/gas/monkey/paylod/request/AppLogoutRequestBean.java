package com.ba.gas.monkey.paylod.request;

import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AppLogoutRequestBean {

    @ValidEntityId(Customer.class)
    private String userId;
    @NotBlank
    private String deviceType;

}
