package com.ba.gas.monkey.paylod.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JwtTokenResponseBean {
    private String token;
    private String type;
    private String refreshToken;
    private String role;
}
