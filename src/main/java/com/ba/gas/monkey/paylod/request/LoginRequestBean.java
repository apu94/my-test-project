package com.ba.gas.monkey.paylod.request;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Data
@ToString
public class LoginRequestBean {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    @NotBlank
    private String token;
    @NotBlank
    private String deviceType;
    private String userType;
}
