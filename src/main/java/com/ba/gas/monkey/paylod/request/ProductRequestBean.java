package com.ba.gas.monkey.paylod.request;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.ProductImage;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ProductRequestBean implements IRequestBodyDTO {
    private String id;
    @NotBlank(message = "Name can't be null")
    private String nameEn;
    @NotBlank(message = "Name can't be null")
    private String nameBn;
    @NotBlank(message = "Description  can't be null")
    private String descriptionEn;
    @NotBlank(message = "Description can't be null")
    private String descriptionBn;
    private String brandId;
    private String productSizeId;
    private String productValveSizeId;
    private Boolean status;
    //for update
    private Boolean featureProduct;
    private Boolean offerProduct;
    private List<ProductImage> productImageList;
}
