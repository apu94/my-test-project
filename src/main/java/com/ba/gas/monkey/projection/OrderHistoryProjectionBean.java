package com.ba.gas.monkey.projection;

import java.time.Instant;

public interface OrderHistoryProjectionBean {
    String getOrderStatus();

    String getOrderAction();

    Instant getActionTime();

    String getResponse();
}
