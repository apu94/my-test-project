package com.ba.gas.monkey.resources.customer.reportissue;

import com.ba.gas.monkey.dtos.reportissue.CreateReportIssueRequestBean;
import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.IssueTypeService;
import com.ba.gas.monkey.services.ReportIssueService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER','PARTNER','DEALER')")
public class AppReportIssueResource {

    private final ReportIssueService reportIssueService;
    private final IssueTypeService issueTypeService;

    @PostMapping("report-issue")
    public ResponseEntity<?> createReportIssue(@RequestBody @Valid CreateReportIssueRequestBean bean) {
        return new ResponseEntity<>(reportIssueService.createIssue(bean), HttpStatus.OK);
    }

    @GetMapping("issue-type-list")
    public ResponseEntity<?> customerIssueTypeList() {
        return new ResponseEntity<>(issueTypeService.getCustomerIssueTypeList(), HttpStatus.OK);
    }

    @GetMapping("report-issue-list/{id}")
    public ResponseEntity<?> reportIssueList(@PathVariable String id) {
        return new ResponseEntity<>(reportIssueService.getReportIssueList(id), HttpStatus.OK);
    }
}
