package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.dtos.brand.BrandReviewBean;
import com.ba.gas.monkey.dtos.partner.PartnerReviewBean;
import com.ba.gas.monkey.services.brand.BrandReviewService;
import com.ba.gas.monkey.services.partner.PartnerReviewService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER')")
public class AppRatingResource {

    private BrandReviewService brandReviewService;
    private PartnerReviewService partnerReviewService;

    @PostMapping("review/brand")
    public ResponseEntity<?> saveBrandReview(@RequestBody @Valid BrandReviewBean bean) {
        return new ResponseEntity<>(brandReviewService.saveReview(bean), HttpStatus.OK);
    }

    @PostMapping("review/partner")
    public ResponseEntity<?> savePartnerReview(@RequestBody @Valid PartnerReviewBean bean) {
        return new ResponseEntity<>(partnerReviewService.saveReview(bean), HttpStatus.OK);
    }

}
