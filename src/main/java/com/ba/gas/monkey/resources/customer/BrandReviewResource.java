package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.services.brand.BrandReviewService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER')")
public class BrandReviewResource {

    private final BrandReviewService brandReviewService;

    @GetMapping("brand/review/{id}")
    public ResponseEntity<?> brandReviewList(@PathVariable("id") String id) {
        return new ResponseEntity<>(brandReviewService.brandReviewList(id), HttpStatus.OK);
    }
}
