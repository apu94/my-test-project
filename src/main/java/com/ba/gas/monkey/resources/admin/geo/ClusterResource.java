package com.ba.gas.monkey.resources.admin.geo;

import com.ba.gas.monkey.dtos.cluster.ClusterCreateBean;
import com.ba.gas.monkey.dtos.district.DistrictBean;
import com.ba.gas.monkey.dtos.thana.ThanaCreateBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.ClusterService;
import com.ba.gas.monkey.services.ThanaService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ClusterResource {

    private final ClusterService clusterService;

    @GetMapping("cluster/{thanaId}")
    public ResponseEntity<?> getDropdownList(@PathVariable String thanaId) {
        return new ResponseEntity<>(clusterService.getDropdownList(thanaId), HttpStatus.OK);
    }

    @GetMapping("cluster-list/{thanaId}")
    public ResponseEntity<?> getClusterList(Pageable pageable,@PathVariable("thanaId") String thanaId) {
        return new ResponseEntity<>(clusterService.getClusterList(pageable,thanaId), HttpStatus.OK);
    }

    @GetMapping("cluster-by-id/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id){
        return new ResponseEntity<>(clusterService.getByOid(id),HttpStatus.OK);
    }

    @PostMapping("cluster")
    public ResponseEntity<?> createCluster(@RequestBody @Validated ClusterCreateBean bean){
        return new ResponseEntity<>(clusterService.createCluster(bean),HttpStatus.OK);
    }

    @DeleteMapping("cluster/{id}")
    public ResponseEntity<?> deleteCluster(@PathVariable("id") String id){
        return new ResponseEntity<>(clusterService.deleteCluster(id),HttpStatus.OK);
    }

    @PutMapping("cluster/{id}")
    public ResponseEntity<?> updateDistrict(@PathVariable("id") String id, @RequestBody @Valid ClusterCreateBean bean){
        return new ResponseEntity<>(clusterService.updateCluster(id,bean),HttpStatus.OK);
    }

}
