package com.ba.gas.monkey.resources.customer.order;

import com.ba.gas.monkey.dtos.order.CreateOrderRequestBean;
import com.ba.gas.monkey.dtos.order.OrderStatusUpdateBean;
import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.CancelReasonService;
import com.ba.gas.monkey.services.order.OrderProductService;
import com.ba.gas.monkey.services.order.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER','PARTNER','DEALER')")
public class OrderResource {

    private final OrderService orderService;
    private final OrderProductService orderProductService;
    private final CancelReasonService cancelReasonService;

    @PostMapping("order")
    public ResponseEntity<?> createOrder(@RequestBody @Valid CreateOrderRequestBean bean) {
        return new ResponseEntity<>(orderService.createOrder(bean), HttpStatus.OK);
    }

    @GetMapping("order-list/{id}")
    public ResponseEntity<?> orderList(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getOrderList(id), HttpStatus.OK);
    }

    @GetMapping("get-order/{id}")
    public ResponseEntity<?> getOrder(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getOrder(id), HttpStatus.OK);
    }

    @PostMapping("order-status-update")
    public ResponseEntity<?> updateOrderStatus(@RequestBody @Valid OrderStatusUpdateBean bean) {
        return new ResponseEntity<>(orderService.updateOrderStatus(bean), HttpStatus.OK);
    }

    @GetMapping("customer-current-order-list/{id}")
    public ResponseEntity<?> customerCurrentOrderList(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getCustomerRunningOrderList(id), HttpStatus.OK);
    }

    @GetMapping("customer-past-order-list/{id}")
    public ResponseEntity<?> customerPastOrderList(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getCustomerPastOrderList(id), HttpStatus.OK);
    }

    @GetMapping("cancel-reason-list")
    public ResponseEntity<?> customerCancelReasonList() {
        return new ResponseEntity<>(cancelReasonService.getCustomerCancelReasonList(), HttpStatus.OK);
    }
}
