package com.ba.gas.monkey.resources.partner;

import com.ba.gas.monkey.dtos.partner.PartnerCreateBean;
import com.ba.gas.monkey.services.partner.PartnerRegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@PartnerV1API
@AllArgsConstructor
public class PartnerRegistrationResource {

    private final PartnerRegistrationService service;

    @PostMapping("registration")
    public ResponseEntity<?> partnerRegistration(@RequestBody @Valid PartnerCreateBean bean) {
        return new ResponseEntity<>(service.partnerRegistration(bean), HttpStatus.OK);
    }
}
