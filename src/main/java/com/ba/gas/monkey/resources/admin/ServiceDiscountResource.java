package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.ServiceDiscountBean;
import com.ba.gas.monkey.services.ServiceDiscountService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@AdminV1API
@AllArgsConstructor
public class ServiceDiscountResource {

    private final ServiceDiscountService service;

    @GetMapping("service-discount")
    public ResponseEntity<?> getList() {
        return new ResponseEntity<>(service.getServiceDiscount(), HttpStatus.OK);
    }


    @PutMapping("service-discount/{id}")
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody ServiceDiscountBean bean) {
        return new ResponseEntity<>(service.updateServiceDiscount(id,bean), HttpStatus.OK);
    }

}
