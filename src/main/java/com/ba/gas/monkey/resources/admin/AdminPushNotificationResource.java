package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.pushnotification.PushNotificationBean;
import com.ba.gas.monkey.services.notification.PushNotificationService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class AdminPushNotificationResource {

    private final PushNotificationService service;

    @PostMapping(value = "push-notification")
    public ResponseEntity<?> createPushNotification(@RequestBody @Valid PushNotificationBean notificationBean) {
        return new ResponseEntity<>(service.createPushNotification(notificationBean), HttpStatus.OK);
    }

    @GetMapping(value = "push-notification")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getPushNotificationList(pageable), HttpStatus.OK);
    }
}
