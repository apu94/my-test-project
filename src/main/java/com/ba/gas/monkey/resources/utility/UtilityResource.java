package com.ba.gas.monkey.resources.utility;

import com.ba.gas.monkey.dtos.DeliveryTimeCalculationBean;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.product.ProductService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.DeliveryScheduleUtils;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;


@AllArgsConstructor
@UtilityV1APIResource
public class UtilityResource {

    private final CustomerService customerService;
    private final UserInfoService userInfoService;
    private final ProductService productService;

    private final DeliveryScheduleUtils deliveryScheduleUtils;

    @GetMapping("check-availability/{phoneNo}")
    public ResponseEntity<?> checkAvailability(@PathVariable("phoneNo") String phoneNo) {
        return new ResponseEntity<>(customerService.checkAvailability(phoneNo), HttpStatus.OK);
    }

    @GetMapping("admin/check-phone/{phoneNo}")
    public ResponseEntity<?> checkMobileAvailability(@PathVariable("phoneNo") String phoneNo) {
        return new ResponseEntity<>(userInfoService.checkMobileAvailability(phoneNo), HttpStatus.OK);
    }

    @GetMapping("admin/check-email/{email}")
    public ResponseEntity<?> checkEmailAvailability(@PathVariable("email") String email) {
        return new ResponseEntity<>(userInfoService.checkEmailAvailability(email), HttpStatus.OK);
    }

    @PostMapping("admin/check-date")
    public ResponseEntity<?> getExpressDeliveryInMinute(@RequestBody DeliveryTimeCalculationBean bean) {
        return new ResponseEntity<>(deliveryScheduleUtils.getExpressDeliveryInMinute(bean.getDateTime(), bean.getSlot()), HttpStatus.OK);
    }

    @GetMapping("product-list-for-web")
    public ResponseEntity<?> productListForWeb() {
        return new ResponseEntity<>(productService.productListForWeb(), HttpStatus.OK);
    }

}
