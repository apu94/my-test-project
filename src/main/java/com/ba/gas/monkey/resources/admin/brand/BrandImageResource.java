package com.ba.gas.monkey.resources.admin.brand;


import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.brand.BrandImageBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.brand.BrandImageService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@AdminV1API
@AllArgsConstructor
public class BrandImageResource {
    private final BrandImageService service;

    @GetMapping("brand-image")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getList(pageable), HttpStatus.OK);
    }

    @GetMapping("brand-image/{id}")
    public ResponseEntity<?> getList(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @DeleteMapping("brand-image/{id}")
    public ResponseEntity<?> deleteImage(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.deleteByOid(id), HttpStatus.OK);
    }

    @PutMapping("brand-image/{id}")
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody BrandImageBean bean) {
        return new ResponseEntity<>(service.updateBrandImage(id,bean), HttpStatus.OK);
    }

    @PostMapping("brand-image")
    public ResponseEntity<?> create(@Validated @RequestBody BrandImageBean brandImageBean) {
        BrandImageBean bean = service.createBrandImage(brandImageBean);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(bean.getId())
                .toUri();
        return ResponseEntity.created(location).body(bean);
    }

    @PutMapping("brand-image/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }

}
