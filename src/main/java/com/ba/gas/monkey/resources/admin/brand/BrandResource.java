package com.ba.gas.monkey.resources.admin.brand;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.brand.BrandBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.brand.BrandService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class BrandResource {
    private final BrandService service;

    @GetMapping("brand")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getBrandList(pageable), HttpStatus.OK);
    }

    @GetMapping("brand/{id}")
    public ResponseEntity<?> getList(@PathVariable String id) {
        return new ResponseEntity<>(service.getBrandDetailsBean(id), HttpStatus.OK);
    }

    @GetMapping("brand/dropdown-list")
    public ResponseEntity<?> dropdownList() {
        return new ResponseEntity<>(service.dropdownList(), HttpStatus.OK);
    }

    @PostMapping("brand")
    public ResponseEntity<?> createBrand(@RequestBody @Valid BrandBean brandBean) {
        return new ResponseEntity<>(service.createBrand(brandBean), HttpStatus.OK);
    }

    @PutMapping("brand/{brandId}")
    public ResponseEntity<?> updateBrand(@PathVariable String brandId, @RequestBody @Valid BrandBean brandBean) {
        return new ResponseEntity<>(service.updateBrand(brandId, brandBean), HttpStatus.OK);
    }

    @PutMapping("brand/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }
}
