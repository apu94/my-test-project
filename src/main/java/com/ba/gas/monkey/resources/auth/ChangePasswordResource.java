package com.ba.gas.monkey.resources.auth;

import com.ba.gas.monkey.dtos.changepassword.ChangePasswordBean;
import com.ba.gas.monkey.services.ChangePasswordService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AuthV1APIResource
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER','PARTNER','DEALER')")
public class ChangePasswordResource {

    private final ChangePasswordService changePasswordService;
    private final AuthenticationManager authenticationManager;

    @PostMapping("change-password")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody ChangePasswordBean passwordBean) {
        return ResponseEntity.ok(changePasswordService.changePassword(passwordBean));
    }

}
