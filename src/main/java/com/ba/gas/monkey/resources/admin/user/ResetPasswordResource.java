package com.ba.gas.monkey.resources.admin.user;

import com.ba.gas.monkey.dtos.ResetPasswordBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.ResetPasswordService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ResetPasswordResource {

    ResetPasswordService resetPasswordService;

    @PostMapping("reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody @Valid ResetPasswordBean bean) {
        resetPasswordService.resetPassword(bean);
        return new ResponseEntity<>("reset password successfully", HttpStatus.OK);
    }
}
