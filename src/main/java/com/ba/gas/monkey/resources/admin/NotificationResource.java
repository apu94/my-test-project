package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.services.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@AdminV1API
@AllArgsConstructor
public class NotificationResource {


    private final NotificationService notificationService;

    @GetMapping("notification-list")
    public ResponseEntity<?> notificationList() {
        return new ResponseEntity<>(notificationService.getNotifications(), HttpStatus.OK);
    }
}
