package com.ba.gas.monkey.resources.dealer;

import com.ba.gas.monkey.services.DealerDashboardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@DealerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('DEALER')")
public class DealerDashboardResource {

    private final DealerDashboardService dealerDashboardService;

    @GetMapping("dashboard")
    public ResponseEntity<?> dealerDashboardData(@RequestParam String date) {
        return new ResponseEntity<>(dealerDashboardService.dashboardData(date), HttpStatus.OK);
    }
}
