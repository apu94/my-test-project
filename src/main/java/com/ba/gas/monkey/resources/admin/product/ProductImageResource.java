package com.ba.gas.monkey.resources.admin.product;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.product.ProductImageBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.product.ProductImageService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@AdminV1API
@AllArgsConstructor
public class ProductImageResource {

    private final ProductImageService service;

    @GetMapping("product-image")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getList(pageable), HttpStatus.OK);
    }

    @GetMapping("product-image/{id}")
    public ResponseEntity<?> getList(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @PostMapping("product-image")
    public ResponseEntity<?> create(@Validated @RequestBody ProductImageBean productImageBean) {
        ProductImageBean bean = service.createProductImage(productImageBean);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(bean.getId())
                .toUri();
        return ResponseEntity.created(location).body(bean);
    }

    @PostMapping("upload/product-image")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        return new ResponseEntity<>(service.saveFile(file), HttpStatus.OK);
    }

    @DeleteMapping("product-image/{id}")
    public ResponseEntity<?> deleteImage(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.deleteByOid(id), HttpStatus.OK);
    }

    @PutMapping("product-image/{id}")
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody ProductImageBean bean) {
        return new ResponseEntity<>(service.updateProductImage(id,bean), HttpStatus.OK);
    }

    @PutMapping("product-image/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }
}
