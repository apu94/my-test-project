package com.ba.gas.monkey.resources.dealer;

import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.dtos.dealer.DealerCreateBean;
import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.customer.CustomerRegistrationService;
import com.ba.gas.monkey.services.dealer.DealerRegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@DealerV1API
@AllArgsConstructor
public class DealerRegistrationResource {

    private final DealerRegistrationService service;

    @PostMapping("registration")
    public ResponseEntity<?> createCustomerRegistration(@RequestBody @Valid DealerCreateBean bean) {
        return new ResponseEntity<>(service.createDealerRegistration(bean), HttpStatus.OK);
    }
}
