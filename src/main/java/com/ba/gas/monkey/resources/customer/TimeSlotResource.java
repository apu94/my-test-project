package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.dtos.timeslot.TimeSlotRequestBean;
import com.ba.gas.monkey.services.DeliveryScheduleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CustomerV1API
@AllArgsConstructor
public class TimeSlotResource {

    private final DeliveryScheduleService deliveryScheduleService;

    @GetMapping("time-slot")
    public ResponseEntity<?> getTimeSlot() {
        return new ResponseEntity<>(deliveryScheduleService.getTimeSlot(), HttpStatus.OK);
    }

    @PostMapping("time-slot")
    public ResponseEntity<?> getTimeSlotForSpecificDate(@RequestBody TimeSlotRequestBean requestBean) {
        return new ResponseEntity<>(deliveryScheduleService.getTimeSlotForSpecificDate(requestBean), HttpStatus.OK);
    }
}
