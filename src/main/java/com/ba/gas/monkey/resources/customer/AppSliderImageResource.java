package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.services.SliderImageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@CustomerV1API
@AllArgsConstructor
public class AppSliderImageResource {

    private final SliderImageService service;

    @GetMapping("slider-image")
    public ResponseEntity<?> getSliderImages() {
        return new ResponseEntity<>(service.getSliderImagesForApp(), HttpStatus.OK);
    }
}
