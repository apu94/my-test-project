package com.ba.gas.monkey.utility;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.TokenUserInfo;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.user.LoggedInUserBean;
import com.ba.gas.monkey.security.admin.UserDetailsImpl;
import com.ba.gas.monkey.security.customer.CustomerDetailsImpl;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.stream.Collectors;

public class AuthenticationUtils {

    public static String getTokenUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<String> role = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toUnmodifiableList());
        String assignedRole = role.get(0);
        if (AppConstant.APP_USER_LIST.contains(assignedRole)) {
            CustomerDetailsImpl userDetails = (CustomerDetailsImpl) authentication.getPrincipal();
            return userDetails.getCustomer().getId();
        }
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return userDetails.getUserInfo().getId();
    }

    public static CustomerBean getCustomerDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getPrincipal().toString().equalsIgnoreCase("anonymousUser")) {
            CustomerDetailsImpl details = (CustomerDetailsImpl) authentication.getPrincipal();
            return details.getCustomer();
        }
        return null;
    }
}
