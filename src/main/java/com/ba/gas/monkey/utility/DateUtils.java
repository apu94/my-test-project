package com.ba.gas.monkey.utility;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

public class DateUtils {

    public static Date getToday() {
        return new Date();
    }

    public static boolean isDateInBetweenIncludingEndPoints(final LocalDate min, final LocalDate max, final LocalDateTime date) {
        return (date.isAfter(min.atStartOfDay()) && date.isBefore(max.plusDays(1).atStartOfDay()));
    }

    public static boolean isDateAfter(final LocalDate min, final LocalDateTime date) {
        return date.isAfter(min.atStartOfDay());
    }

    public static LocalDateTime getLocalDateTimeFromInstant(Instant instant) {
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public static String getDateFromInstant(Instant instant, String format) {
        if (Objects.isNull(instant)) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.systemDefault());
        return formatter.format(instant);
    }
}
