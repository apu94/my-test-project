package com.ba.gas.monkey.utility;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.security.admin.UserDetailsImpl;
import com.ba.gas.monkey.services.RoleMenuService;
import io.jsonwebtoken.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
public class JwtUtils {

    public String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
        return Jwts.builder()
                .setIssuer(AppConstant.JWT_TOKEN_ISSUER)
                .setSubject(AppConstant.JWT_TOKEN_SUBJECT)
                .claim("userId", userPrincipal.getUserInfo().getId())
                .claim("username", userPrincipal.getUsername())
                .claim("authorities", populateAuthorities(userPrincipal.getAuthorities()))
                .claim("accessMenus", userPrincipal.getUserInfo().getMenus())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + AppConstant.JWT_TOKEN_EXPIRE_IN))
                .signWith(SignatureAlgorithm.HS512, AppConstant.JWT_TOKEN_SECRET)
                .compact();
    }

    public String generateTokenFromUsername(UserInfoBean userInfoBean) {
        Set<String> authorities = new HashSet<>(userInfoBean.getAuthorities());
        return Jwts.builder().setSubject(AppConstant.JWT_TOKEN_SUBJECT)
                .claim("userId", userInfoBean.getId())
                .claim("username", userInfoBean.getEmail())
                .claim("authorities", String.join(",", authorities))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + AppConstant.JWT_TOKEN_EXPIRE_IN))
                .signWith(SignatureAlgorithm.HS512, AppConstant.JWT_TOKEN_SECRET)
                .compact();
    }

    public String generateTokenForApp(CustomerBean userInfoBean) {
        Set<String> authorities = new HashSet<>(userInfoBean.getAuthorities());
        return Jwts.builder().setSubject(AppConstant.JWT_TOKEN_SUBJECT)
                .claim("userId", userInfoBean.getId())
                .claim("username", userInfoBean.getPhoneNo())
                .claim("authorities", String.join(",", authorities))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + AppConstant.JWT_TOKEN_EXPIRE_IN))
                .signWith(SignatureAlgorithm.HS512, AppConstant.JWT_TOKEN_SECRET)
                .compact();
    }

    public String getUserNameFromJwtToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(AppConstant.JWT_TOKEN_SECRET).parseClaimsJws(token).getBody();
        return claims.get("username").toString();
    }

    public String getAuthoritiesFromJwtToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(AppConstant.JWT_TOKEN_SECRET).parseClaimsJws(token).getBody();
        return claims.get("authorities").toString();
    }

    private String populateAuthorities(Collection<? extends GrantedAuthority> collection) {
        Set<String> authoritiesSet = new HashSet<>();
        for (GrantedAuthority authority : collection) {
            authoritiesSet.add(authority.getAuthority());
        }
        return String.join(",", authoritiesSet);
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(AppConstant.JWT_TOKEN_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.error("Invalid JWT Signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage());
            throw new ServiceExceptionHolder.CustomException(AppConstant.TOKEN_EXPIRED_EXCEPTION, e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
