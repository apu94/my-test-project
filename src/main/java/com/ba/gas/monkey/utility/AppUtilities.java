package com.ba.gas.monkey.utility;

import java.util.concurrent.atomic.AtomicLong;

public class AppUtilities {
    private static final AtomicLong sequence = new AtomicLong(System.currentTimeMillis() / 1000);

    public static long getNext() {
        return sequence.incrementAndGet();
    }


}
