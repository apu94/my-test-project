package com.ba.gas.monkey.utility;

import com.ba.gas.monkey.models.DeliverySchedule;
import com.ba.gas.monkey.services.DeliveryScheduleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service("deliveryScheduleUtils")
public class DeliveryScheduleUtils {

    private final DeliveryScheduleService scheduleService;

    public int getExpressDeliveryInMinute(LocalDate localDate, String slot) {
        String[] strings = slot.split("-");
        System.out.println("Express localDate:------------" + localDate);
        DateFormat format = new SimpleDateFormat("hh:mm a");
        LocalDateTime nextTime = null;
        try {
            nextTime = format.parse(strings[1]).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert nextTime != null;
        LocalDateTime currentTime = LocalDateTime.of(localDate, LocalTime.now());
        LocalDateTime localDateTime = Instant.now().atZone(ZoneId.systemDefault()).toLocalDateTime().withHour(nextTime.getHour()).withMinute(nextTime.getMinute());
        return (int) ChronoUnit.MINUTES.between(currentTime, localDateTime);
    }

    public int getNextOpeningTime(LocalDateTime deliveryTime) {
        System.out.println("delivery time: " + deliveryTime);
        List<DeliverySchedule> list = scheduleService.getRepository().findAll();
        if (!list.isEmpty()) {
            DeliverySchedule schedule = list.get(0);
            LocalTime endTime = schedule.getEndTime();
            System.out.println("End time: " + endTime);
            LocalDateTime endScheduleTime = LocalDateTime.now().withHour(endTime.getHour()).withMinute(endTime.getMinute());
            System.out.println("End schedule time: " + endScheduleTime);
            if (deliveryTime.isBefore(endScheduleTime)) {
                int min = (int) Duration.between(deliveryTime, endScheduleTime).toMinutes();
                System.out.println("Next opening min left:------------" + min);
                return min;
            }
            LocalTime startTime = schedule.getStartTime();
            System.out.println("start time: " + startTime);
            LocalDateTime startScheduleTime = LocalDateTime.now().plusDays(1).withHour(startTime.getHour()).withMinute(startTime.getMinute());
            System.out.println("start schedule time: " + startTime);
            int min = (int) Duration.between(deliveryTime, startScheduleTime).toMinutes();
            System.out.println("Next opening min left:------------" + min);
            return min;
        }
        return 0;
    }

    public boolean scheduleCheck(LocalDateTime deliveryTime) {
        List<DeliverySchedule> list = scheduleService.getRepository().findAll();
        if (!list.isEmpty()) {
            DeliverySchedule schedule = list.get(0);
            LocalTime startTime = schedule.getEndTime();
            LocalDateTime endScheduleTime = LocalDateTime.now().withHour(startTime.getHour()).withMinute(startTime.getMinute());
            boolean scheduleCheck = deliveryTime.isBefore(endScheduleTime);
            log.info("Schedule check:------------{}", scheduleCheck);
            return scheduleCheck;
        }
        log.info("Schedule check:------------{}", Boolean.FALSE);
        return Boolean.FALSE;
    }
}
