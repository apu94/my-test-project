package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.ProductImage;
import com.ba.gas.monkey.models.UserInfo;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProductImageRepository extends BaseRepository<ProductImage> {

}
