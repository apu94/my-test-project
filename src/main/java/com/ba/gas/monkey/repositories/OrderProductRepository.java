package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.models.OrderProduct;
import com.ba.gas.monkey.models.Product;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderProductRepository extends BaseRepository<OrderProduct> {
    List<OrderProduct> findByOrderInfo(OrderInfo orderInfo);

    @Query("SELECT DISTINCT o.buyProduct FROM OrderProduct o")
    List<Product> findAllDistinctBuyProducts();
}
