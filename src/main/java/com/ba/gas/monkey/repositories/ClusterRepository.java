package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Cluster;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.models.Thana;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClusterRepository extends BaseRepository<Cluster> {

    List<Cluster> findByThana(Thana thana);

    Page<Cluster> findByThana(Thana thana, Pageable pageable);
}
