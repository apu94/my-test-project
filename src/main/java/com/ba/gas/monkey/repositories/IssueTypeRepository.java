package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.IssueType;

public interface IssueTypeRepository extends BaseRepository<IssueType>  {

}
