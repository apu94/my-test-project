package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Cart;

import java.util.Optional;

public interface CartRepository extends BaseRepository<Cart> {
    Optional<Cart>  findByCustomerId(String id);
}
