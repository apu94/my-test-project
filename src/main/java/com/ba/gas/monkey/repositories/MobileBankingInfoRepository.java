package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.MobileBankingInfo;

public interface MobileBankingInfoRepository extends BaseRepository<MobileBankingInfo>  {

}
